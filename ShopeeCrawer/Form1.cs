﻿using DevExpress.XtraPrinting.HtmlExport.Native;

using Newtonsoft.Json;

using OfficeOpenXml;

using RestSharp;

using ShopeeCrawer.Core;
using ShopeeCrawer.Model;
using ShopeeCrawer.Properties;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace ShopeeCrawer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // private string _url = "https://shopee.vn/miso2312";
        private readonly string _urlBase = "https://shopee.vn";
        private const string GetProductUrl =
          // "/api/v2/search_items/?by={0}&limit={1}&match_id={2}&newest={3}&order={4}&page_type=shop";
          "/api/v4/search/search_items?by={0}&limit={1}&match_id={2}&order={3}&page_type=shop&limit=20&offset={4}&version=2";

        // private const string UrlProductDetail = "/api/v1/item_detail/?item_id={0}&shop_id={1}";
        private const string UrlProductDetail = "/api/v4/item/get?item_id={0}&shop_id={1}";
        private const string UrlImage = "https://cf.shopee.vn/file/{0}";
        private RestClient _restClient;

        private readonly List<ProductDetailViewModel> _lstDetail = new List<ProductDetailViewModel>();
        private void Form1_Load(object sender, EventArgs e)
        {
            _restClient = new RestClient(ShopeeCrawer.Properties.Resources.ShopeeBaseUrl);
        }
        private void btnGetdata_Click(object sender, EventArgs e)
        {
            try
            {
                splashScreenManager1.ShowWaitForm();
                //Lay thong tin shop
                splashScreenManager1.SetWaitFormDescription("Đang tải dang sách sản phẩm....");
                for (int i = int.Parse(txtFromPages.Value.ToString(CultureInfo.InvariantCulture)); i < txtToPage.Value; i++)
                {
                    decimal newest = i * txtLimit.Value;
                    request = new RestRequest(_urlBase + string.Format(
                        GetProductUrl,
                        txtBy.Text.Trim(),
                        txtLimit.Text.Trim(),
                        txtShopId.Text.Trim(),
                        txtOrder.Text.Trim(),
                        newest));
                    request.AddHeader("Content-Type", "application/json");
                    request.AddHeader("timestamp", DateTime.Now.Ticks.ToString());
                    IRestResponse response = _restClient.Get(request);
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        MessageBox.Show("Lỗi khi lấy danh sách sản phẩm");
                        Cursor = Cursors.Default;

                        return;
                    }
                    ProductListData result = JsonConvert.DeserializeObject<ProductListData>(response.Content,
                        new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });


                    if (result.Items.Count > 0)
                    {
                        foreach (var dataItem in result.Items)
                        {
                            LoadProductDetail(dataItem);
                        }
                        Thread.Sleep(10000);
                    }
                    else
                    {
                        break;
                    }


                }


                bdsData.DataSource = _lstDetail;
                bdsData.ResetBindings(false);
                splashScreenManager1.CloseWaitForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Resources.Form1_btnGetdata_Click_Error_Message);

            }

        }

        private RestRequest request = new RestRequest();

        private void LoadProductDetail(Item dataItem)
        {

            request = new RestRequest
            {
                Resource = _urlBase + string.Format(UrlProductDetail, dataItem.Itemid, txtShopId.Text.Trim())
            };
            //Lay thong tin shop
            //request.AddHeader("referer", "https://shopee.vn/miso2312");
            request.AddHeader("referer", string.Join("", "https://shopee.vn/", DXHttpUtility.UrlEncodeToUnicodeCompatible(dataItem.ItemBasic.Name), "-i.", txtShopId.Text.Trim(), ".", dataItem.Itemid));
            //request.AddHeader("Content-Type", "application/json");
            request.AddHeader("accept", "*/*");
            request.AddHeader("x-api-source", "pc");
            request.AddHeader("timestamp", DateTime.Now.Ticks.ToString());
            //  request.AddHeader("DEBUG", "True");
            //   request.AddJsonBody(new {usernames = new string[]{"miso2312"}});
            IRestResponse response = _restClient.Get(request);
            Debug.WriteLine("Get: " + request.Resource);
            ProductDetailData proDetail = JsonConvert.DeserializeObject<ProductDetailData>(response.Content,
                         new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });

            //var lstItem = JsonConvert.DeserializeObject<ProductDetailData>(response.Content);
            ProductDetailViewModel viewModel = new ProductDetailViewModel();
            //if (proDetail.Categories != null && proDetail.Categories.Count > 0)
            //{
            //    viewModel.ps_category_list_id = string.Join(",", proDetail.Categories);
            //}

            viewModel.ps_product_name = proDetail.Data.Name;
            viewModel.ps_product_description = proDetail.Data.Description;
            viewModel.ps_price = proDetail.Data.Price;
            viewModel.ps_stock = proDetail.Data.Stock ?? 0;
            //Không lấy được khối lượng
            viewModel.ps_product_weight = 50;
            viewModel.ps_days_to_ship = 2;
            //Mã SP 
            //viewModel.ps_sku_ref_no_parent = proDetail.Name;
            SetModel(viewModel, proDetail);
            SetImage(viewModel, proDetail);
            //viewModel.ps_mass_upload_shipment_help = proDetail.Name;
            //Mặc định mở tất cả đơn vị vân chuyển
            //viewModel.channel_50010_switch = "Mở";
            //viewModel.channel_50011_switch = "Mở";
            //viewModel.channel_50012_switch = "Mở";
            //viewModel.channel_50066_switch = "Mở";

            _lstDetail.Add(viewModel);

        }

        private void SetImage(ProductDetailViewModel viewModel, ProductDetailData proDetail)
        {
            viewModel.ImageIds = proDetail.Data.Images.ToList();
            if (viewModel.ImageIds.Count > 0)
            {
                viewModel.ps_img_1 = "https://cf.shopee.vn/file/" + viewModel.ImageIds[0];
                if (viewModel.ImageIds.Count < 2)
                {
                    return;
                }

                viewModel.ps_img_2 = "https://cf.shopee.vn/file/" + viewModel.ImageIds[1];
                if (viewModel.ImageIds.Count < 3)
                {
                    return;
                }

                viewModel.ps_img_3 = "https://cf.shopee.vn/file/" + viewModel.ImageIds[2];
                if (viewModel.ImageIds.Count < 4)
                {
                    return;
                }

                viewModel.ps_img_4 = "https://cf.shopee.vn/file/" + viewModel.ImageIds[3];
                if (viewModel.ImageIds.Count < 5)
                {
                    return;
                }

                viewModel.ps_img_5 = "https://cf.shopee.vn/file/" + viewModel.ImageIds[4];
                if (viewModel.ImageIds.Count < 6)
                {
                    return;
                }

                viewModel.ps_img_6 = "https://cf.shopee.vn/file/" + viewModel.ImageIds[5];
                if (viewModel.ImageIds.Count < 7)
                {
                    return;
                }

                viewModel.ps_img_7 = "https://cf.shopee.vn/file/" + viewModel.ImageIds[6];
                if (viewModel.ImageIds.Count < 8)
                {
                    return;
                }

                viewModel.ps_img_8 = "https://cf.shopee.vn/file/" + viewModel.ImageIds[7];
                if (viewModel.ImageIds.Count < 9)
                {
                    return;
                }

                viewModel.ps_img_9 = "https://cf.shopee.vn/file/" + viewModel.ImageIds[8];

            }
        }

        private void SetModel(ProductDetailViewModel viewModel, ProductDetailData proDetail)
        {
            //viewModel.ps_mass_upload_variation_help = proDetail.Name;
            if (proDetail.Data.Models.Count() >= 1)
            {
                viewModel.ps_variation_1_ps_variation_sku = proDetail.Data.Models[0].ModelSku;
                viewModel.ps_variation_1_ps_variation_name = proDetail.Data.Models[0].Name;
                viewModel.ps_variation_1_ps_variation_price = proDetail.Data.Models[0].Price ?? proDetail.Data.Price.GetValueOrDefault();
                viewModel.ps_variation_1_ps_variation_stock = proDetail.Data.Models[0].Stock ?? proDetail.Data.Stock.GetValueOrDefault();
                if (proDetail.Data.Models.Count < 2)
                {
                    return;
                }

                viewModel.ps_variation_1_ps_variation_sku = proDetail.Data.Models[1].ModelSku;
                viewModel.ps_variation_2_ps_variation_name = proDetail.Data.Models[1].Name;
                viewModel.ps_variation_2_ps_variation_price = proDetail.Data.Models[1].Price ?? proDetail.Data.Price.GetValueOrDefault(); ;
                viewModel.ps_variation_2_ps_variation_stock = proDetail.Data.Models[1].Stock ?? proDetail.Data.Stock.GetValueOrDefault(); ;
                if (proDetail.Data.Models.Count < 3)
                {
                    return;
                }

                viewModel.ps_variation_3_ps_variation_sku = proDetail.Data.Models[2].ModelSku;
                viewModel.ps_variation_3_ps_variation_name = proDetail.Data.Models[2].Name;
                viewModel.ps_variation_3_ps_variation_price = proDetail.Data.Models[2].Price ?? proDetail.Data.Price.GetValueOrDefault(); ;
                viewModel.ps_variation_3_ps_variation_stock = proDetail.Data.Models[2].Stock ?? proDetail.Data.Stock.GetValueOrDefault(); ;
                if (proDetail.Data.Models.Count < 4)
                {
                    return;
                }

                viewModel.ps_variation_4_ps_variation_sku = proDetail.Data.Models[3].ModelSku;
                viewModel.ps_variation_4_ps_variation_name = proDetail.Data.Models[3].Name;
                viewModel.ps_variation_4_ps_variation_price = proDetail.Data.Models[3].Price ?? proDetail.Data.Price.GetValueOrDefault(); ;
                viewModel.ps_variation_4_ps_variation_stock = proDetail.Data.Models[3].Stock ?? proDetail.Data.Stock.GetValueOrDefault(); ;
                if (proDetail.Data.Models.Count < 5)
                {
                    return;
                }

                viewModel.ps_variation_5_ps_variation_sku = proDetail.Data.Models[4].ModelSku;
                viewModel.ps_variation_5_ps_variation_name = proDetail.Data.Models[4].Name;
                viewModel.ps_variation_5_ps_variation_price = proDetail.Data.Models[4].Price ?? proDetail.Data.Price.GetValueOrDefault(); ;
                viewModel.ps_variation_5_ps_variation_stock = proDetail.Data.Models[4].Stock ?? proDetail.Data.Stock.GetValueOrDefault(); ;
                if (proDetail.Data.Models.Count < 6)
                {
                    return;
                }

                viewModel.ps_variation_6_ps_variation_sku = proDetail.Data.Models[5].ModelSku;
                viewModel.ps_variation_6_ps_variation_name = proDetail.Data.Models[5].Name;
                viewModel.ps_variation_6_ps_variation_price = proDetail.Data.Models[5].Price ?? proDetail.Data.Price.GetValueOrDefault(); ;
                viewModel.ps_variation_6_ps_variation_stock = proDetail.Data.Models[5].Stock ?? proDetail.Data.Stock.GetValueOrDefault(); ;
                if (proDetail.Data.Models.Count < 7)
                {
                    return;
                }

                viewModel.ps_variation_7_ps_variation_sku = proDetail.Data.Models[6].ModelSku;
                viewModel.ps_variation_7_ps_variation_name = proDetail.Data.Models[6].Name;
                viewModel.ps_variation_7_ps_variation_price = proDetail.Data.Models[6].Price ?? proDetail.Data.Price.GetValueOrDefault();
                viewModel.ps_variation_7_ps_variation_stock = proDetail.Data.Models[6].Stock ?? proDetail.Data.Stock.GetValueOrDefault();
                if (proDetail.Data.Models.Count < 8)
                {
                    return;
                }

                viewModel.ps_variation_8_ps_variation_sku = proDetail.Data.Models[7].ModelSku;
                viewModel.ps_variation_8_ps_variation_name = proDetail.Data.Models[7].Name;
                viewModel.ps_variation_8_ps_variation_price = proDetail.Data.Models[7].Price ?? proDetail.Data.Price.GetValueOrDefault();
                viewModel.ps_variation_8_ps_variation_stock = proDetail.Data.Models[7].Stock ?? proDetail.Data.Stock.GetValueOrDefault();
                if (proDetail.Data.Models.Count < 9)
                {
                    return;
                }

                viewModel.ps_variation_9_ps_variation_sku = proDetail.Data.Models[8].ModelSku;
                viewModel.ps_variation_9_ps_variation_name = proDetail.Data.Models[8].Name;
                viewModel.ps_variation_9_ps_variation_price = proDetail.Data.Models[8].Price ?? proDetail.Data.Price.GetValueOrDefault();
                viewModel.ps_variation_9_ps_variation_stock = proDetail.Data.Models[8].Stock ?? proDetail.Data.Stock.GetValueOrDefault();
                if (proDetail.Data.Models.Count < 10)
                {
                    return;
                }

                viewModel.ps_variation_10_ps_variation_sku = proDetail.Data.Models[9].ModelSku;
                viewModel.ps_variation_10_ps_variation_name = proDetail.Data.Models[9].Name;
                viewModel.ps_variation_10_ps_variation_price = proDetail.Data.Models[9].Price ?? proDetail.Data.Price.GetValueOrDefault();
                viewModel.ps_variation_10_ps_variation_stock = proDetail.Data.Models[9].Stock ?? proDetail.Data.Stock.GetValueOrDefault();
                if (proDetail.Data.Models.Count < 11)
                {
                    return;
                }

            }
        }


        private async void button2_Click(object sender, EventArgs e)
        {
            //xuất ra excel
            FolderBrowserDialog openFolder = new FolderBrowserDialog();

            DialogResult result = openFolder.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                Cursor = Cursors.WaitCursor;
                string fileName = "Data Export " + DateTime.Now.ToString("dd-MM-yyyy") + ".xls";
                string xlsPath = Path.Combine(openFolder.SelectedPath + Path.DirectorySeparatorChar + fileName);
                //grvData.ExportToXls(xlsPath);
                ExcelPackage excel = new ExcelPackage(new FileInfo(xlsPath));
                ExcelWorksheet data = excel.Workbook.Worksheets.Add("Exported Data");
                data.Cells[1, 1].LoadFromCollection(_lstDetail, true);
                foreach (ExcelRangeBase b in data.Cells.Where(c => c.Value is decimal && (decimal)c.Value == 0))
                {
                    b.Value = null;

                }
                excel.Save();

                // Image URL 
                // https://cf.shopee.vn/file/fbd3bba687ecb7aee53eda7b0b0c7332

                List<Task> lstT = new List<Task>();
                foreach (ProductDetailViewModel item in _lstDetail)
                {
                    Task t = Task.Run(() =>
                    {

                        string folderName = "";
                        folderName = item.ps_product_name.RemoveWhiteSpace().NonUnicode().RemoveNonAphabetNumberic();
                        if (folderName.Length > 100)
                        {
                            folderName = folderName.Substring(0, 100);
                        }

                        string folderPath = openFolder.SelectedPath + Path.DirectorySeparatorChar + folderName;
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }

                        foreach (string imageId in item.ImageIds)
                        {
                            try
                            {
                                WebClient client = new WebClient();
                                client.DownloadFile(string.Format(UrlImage, imageId),
                                    folderPath + Path.DirectorySeparatorChar + imageId + ".jpg");

                                client.Dispose();
                            }
                            catch
                            {
                                try
                                {
                                    WebClient client = new WebClient();
                                    client.DownloadFile(string.Format(UrlImage, imageId),
                                        folderPath + Path.DirectorySeparatorChar + imageId + ".jpg");
                                    client.Dispose();
                                }
                                catch
                                {

                                }
                            }
                        }

                    });

                    lstT.Add(t);
                }

                await Task.WhenAll(lstT);
                Cursor = Cursors.Default;
                DialogResult re = MessageBox.Show("Xuất dữ liệu thành công. Bạn có  muốn mở thư mục ?", "", MessageBoxButtons.YesNo);
                if (re == DialogResult.Yes)
                {
                    Process.Start(openFolder.SelectedPath);
                }
            }
            openFolder.Dispose();
        }

        private void Form1_HelpButtonClicked(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }
        /// <summary>
        /// Get shop Id when press Enter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtShopName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //request = new RestRequest();
                ////Lay thong tin shop
                //request.Resource = _urlBase + "/api/v1/shop_ids_by_username";
                ////request.AddHeader("referer", "https://shopee.vn/miso2312");
                //request.AddHeader("referer", string.Join("", _urlBase, "/",  txtShopName.Text.Trim()));
                //request.AddHeader("Content-Type", "application/json");
                //request.AddHeader("accept", "application/json");
                //request.AddHeader("x-api-source", "pc");
                //request.AddHeader("timestamp", DateTime.Now.Ticks.ToString());
                ////  request.AddHeader("DEBUG", "True");
                //   request.AddJsonBody(new {usernames = new string[]{txtShopName.Text.Trim()}});
                //IRestResponse response = _restClient.Post(request);
                //Debug.WriteLine("Get: " + request.Resource);
                //ProductDetailData proDetail = JsonConvert.DeserializeObject<ProductDetailData>(response.Content,
                //    new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore });
            }
        }
    }

}