﻿// To parse this JS

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

using System.Collections.Generic;
using System.Globalization;

namespace ShopeeCrawer.Model
{
    public partial class ProductDetailData
    {
        [JsonProperty("error")]
        public object Error { get; set; }

        [JsonProperty("error_msg")]
        public object ErrorMsg { get; set; }

        [JsonProperty("data")]
        public Data Data { get; set; }

        [JsonProperty("is_indexable")]
        public bool IsIndexable { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("itemid")]
        public long Itemid { get; set; }

        [JsonProperty("shopid")]
        public long Shopid { get; set; }

        [JsonProperty("userid")]
        public long Userid { get; set; }

        [JsonProperty("price_max_before_discount")]
        public long PriceMaxBeforeDiscount { get; set; }

        [JsonProperty("has_lowest_price_guarantee")]
        public bool HasLowestPriceGuarantee { get; set; }

        [JsonProperty("price_before_discount")]
        public long PriceBeforeDiscount { get; set; }

        [JsonProperty("price_min_before_discount")]
        public long PriceMinBeforeDiscount { get; set; }

        [JsonProperty("exclusive_price_info")]
        public object ExclusivePriceInfo { get; set; }

        [JsonProperty("hidden_price_display")]
        public object HiddenPriceDisplay { get; set; }

        [JsonProperty("price_min")]
        public long PriceMin { get; set; }

        [JsonProperty("price_max")]
        public long PriceMax { get; set; }

        [JsonProperty("price")]
        public long? Price { get; set; }

        [JsonProperty("stock")]
        public long? Stock { get; set; }

        [JsonProperty("discount")]
        public string Discount { get; set; }

        [JsonProperty("historical_sold")]
        public long HistoricalSold { get; set; }

        [JsonProperty("sold")]
        public long Sold { get; set; }

        [JsonProperty("show_discount")]
        public long ShowDiscount { get; set; }

        [JsonProperty("raw_discount")]
        public long RawDiscount { get; set; }

        [JsonProperty("min_purchase_limit")]
        public long MinPurchaseLimit { get; set; }

        [JsonProperty("overall_purchase_limit")]
        public OverallPurchaseLimit OverallPurchaseLimit { get; set; }

        [JsonProperty("pack_size")]
        public object PackSize { get; set; }

        [JsonProperty("is_live_streaming_price")]
        public object IsLiveStreamingPrice { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("ctime")]
        public long Ctime { get; set; }

        [JsonProperty("item_status")]
        public string ItemStatus { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("condition")]
        public long Condition { get; set; }

        [JsonProperty("catid")]
        public long Catid { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("is_mart")]
        public bool IsMart { get; set; }

        [JsonProperty("rich_text_description")]
        public object RichTextDescription { get; set; }

        [JsonProperty("show_shopee_verified_label")]
        public bool ShowShopeeVerifiedLabel { get; set; }

        [JsonProperty("size_chart")]
        public string SizeChart { get; set; }

        [JsonProperty("reference_item_id")]
        public string ReferenceItemId { get; set; }

        [JsonProperty("brand")]
        public object Brand { get; set; }

        [JsonProperty("item_rating")]
        public ItemRating ItemRating { get; set; }

        [JsonProperty("label_ids")]
        public long[] LabelIds { get; set; }

        [JsonProperty("attributes")]
        public Attribute[] Attributes { get; set; }

        [JsonProperty("liked")]
        public bool Liked { get; set; }

        [JsonProperty("liked_count")]
        public long LikedCount { get; set; }

        [JsonProperty("cmt_count")]
        public long CmtCount { get; set; }

        [JsonProperty("flag")]
        public long Flag { get; set; }

        [JsonProperty("shopee_verified")]
        public bool ShopeeVerified { get; set; }

        [JsonProperty("is_adult")]
        public bool IsAdult { get; set; }

        [JsonProperty("is_preferred_plus_seller")]
        public bool IsPreferredPlusSeller { get; set; }

        [JsonProperty("tier_variations")]
        public TierVariation[] TierVariations { get; set; }

        [JsonProperty("bundle_deal_id")]
        public long BundleDealId { get; set; }

        [JsonProperty("can_use_bundle_deal")]
        public bool CanUseBundleDeal { get; set; }

        [JsonProperty("size_chart_info")]
        public object SizeChartInfo { get; set; }

        [JsonProperty("add_on_deal_info")]
        public object AddOnDealInfo { get; set; }

        [JsonProperty("bundle_deal_info")]
        public BundleDealInfo BundleDealInfo { get; set; }

        [JsonProperty("can_use_wholesale")]
        public bool CanUseWholesale { get; set; }

        [JsonProperty("wholesale_tier_list")]
        public object[] WholesaleTierList { get; set; }

        [JsonProperty("is_group_buy_item")]
        public object IsGroupBuyItem { get; set; }

        [JsonProperty("group_buy_info")]
        public object GroupBuyInfo { get; set; }

        [JsonProperty("welcome_package_type")]
        public long WelcomePackageType { get; set; }

        [JsonProperty("welcome_package_info")]
        public object WelcomePackageInfo { get; set; }

        [JsonProperty("tax_code")]
        public object TaxCode { get; set; }

        [JsonProperty("invoice_option")]
        public object InvoiceOption { get; set; }

        [JsonProperty("complaint_policy")]
        public object ComplaintPolicy { get; set; }

        [JsonProperty("wp_eligibility")]
        public object WpEligibility { get; set; }

        [JsonProperty("images")]
        public string[] Images { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("video_info_list")]
        public VideoInfoList[] VideoInfoList { get; set; }

        [JsonProperty("item_type")]
        public long ItemType { get; set; }

        [JsonProperty("is_official_shop")]
        public bool IsOfficialShop { get; set; }

        [JsonProperty("show_official_shop_label_in_title")]
        public bool ShowOfficialShopLabelInTitle { get; set; }

        [JsonProperty("shop_location")]
        public string ShopLocation { get; set; }

        [JsonProperty("coin_earn_label")]
        public object CoinEarnLabel { get; set; }

        [JsonProperty("cb_option")]
        public long CbOption { get; set; }

        [JsonProperty("is_pre_order")]
        public bool IsPreOrder { get; set; }

        [JsonProperty("estimated_days")]
        public long EstimatedDays { get; set; }

        [JsonProperty("badge_icon_type")]
        public long BadgeIconType { get; set; }

        [JsonProperty("show_free_shipping")]
        public bool ShowFreeShipping { get; set; }

        [JsonProperty("shipping_icon_type")]
        public long ShippingIconType { get; set; }

        [JsonProperty("cod_flag")]
        public long CodFlag { get; set; }

        [JsonProperty("is_service_by_shopee")]
        public bool IsServiceByShopee { get; set; }

        [JsonProperty("show_original_guarantee")]
        public bool ShowOriginalGuarantee { get; set; }

        [JsonProperty("categories")]
        public Category[] Categories { get; set; }

        [JsonProperty("other_stock")]
        public long OtherStock { get; set; }

        [JsonProperty("item_has_post")]
        public bool ItemHasPost { get; set; }

        [JsonProperty("discount_stock")]
        public long DiscountStock { get; set; }

        [JsonProperty("current_promotion_has_reserve_stock")]
        public bool CurrentPromotionHasReserveStock { get; set; }

        [JsonProperty("current_promotion_reserved_stock")]
        public long CurrentPromotionReservedStock { get; set; }

        [JsonProperty("normal_stock")]
        public long NormalStock { get; set; }

        [JsonProperty("brand_id")]
        public long BrandId { get; set; }

        [JsonProperty("is_alcohol_product")]
        public bool IsAlcoholProduct { get; set; }

        [JsonProperty("show_recycling_info")]
        public bool ShowRecyclingInfo { get; set; }

        [JsonProperty("coin_info")]
        public CoinInfo CoinInfo { get; set; }

        [JsonProperty("models")]
        public List<Model> Models { get; set; }

        [JsonProperty("spl_info")]
        public SplInfo SplInfo { get; set; }

        [JsonProperty("preview_info")]
        public object PreviewInfo { get; set; }

        [JsonProperty("fe_categories")]
        public Category[] FeCategories { get; set; }

        [JsonProperty("presale_info")]
        public object PresaleInfo { get; set; }

        [JsonProperty("show_best_price_guarantee")]
        public bool ShowBestPriceGuarantee { get; set; }

        [JsonProperty("is_cc_installment_payment_eligible")]
        public bool IsCcInstallmentPaymentEligible { get; set; }

        [JsonProperty("is_non_cc_installment_payment_eligible")]
        public bool IsNonCcInstallmentPaymentEligible { get; set; }

        [JsonProperty("flash_sale")]
        public object FlashSale { get; set; }

        [JsonProperty("upcoming_flash_sale")]
        public object UpcomingFlashSale { get; set; }

        [JsonProperty("deep_discount")]
        public object DeepDiscount { get; set; }

        [JsonProperty("has_low_fulfillment_rate")]
        public bool HasLowFulfillmentRate { get; set; }

        [JsonProperty("is_partial_fulfilled")]
        public bool IsPartialFulfilled { get; set; }

        [JsonProperty("makeups")]
        public object Makeups { get; set; }

        [JsonProperty("shop_vouchers")]
        public ShopVoucher[] ShopVouchers { get; set; }

        [JsonProperty("credit_insurance_data")]
        public CreditInsuranceData CreditInsuranceData { get; set; }

        [JsonProperty("global_sold")]
        public long GlobalSold { get; set; }

        [JsonProperty("is_infant_milk_formula_product")]
        public bool IsInfantMilkFormulaProduct { get; set; }

        [JsonProperty("should_show_amp_tag")]
        public bool ShouldShowAmpTag { get; set; }

        [JsonProperty("sorted_variation_image_index_list")]
        public object SortedVariationImageIndexList { get; set; }

        [JsonProperty("lowest_past_price")]
        public object LowestPastPrice { get; set; }
    }

    public partial class Attribute
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("is_timestamp")]
        public bool IsTimestamp { get; set; }

        [JsonProperty("brand_option")]
        public object BrandOption { get; set; }

        [JsonProperty("val_id")]
        public object ValId { get; set; }
    }

    public partial class BundleDealInfo
    {
        //[JsonProperty("bundle_deal_id")]
        //public long? BundleDealId { get; set; }

        //[JsonProperty("bundle_deal_label")]
        //public string BundleDealLabel { get; set; }
    }

    public partial class Category
    {
        [JsonProperty("catid")]
        public long Catid { get; set; }

        [JsonProperty("display_name")]
        public string DisplayName { get; set; }

        [JsonProperty("no_sub")]
        public bool NoSub { get; set; }

        [JsonProperty("is_default_subcat")]
        public bool IsDefaultSubcat { get; set; }
    }

    public partial class CoinInfo
    {
        [JsonProperty("spend_cash_unit")]
        public long SpendCashUnit { get; set; }

        [JsonProperty("coin_earn_items")]
        public object[] CoinEarnItems { get; set; }
    }

    public partial class CreditInsuranceData
    {
        [JsonProperty("insurance_products")]
        public object[] InsuranceProducts { get; set; }
    }

    public partial class ItemRating
    {
        [JsonProperty("rating_star")]
        public double RatingStar { get; set; }

        [JsonProperty("rating_count")]
        public long[] RatingCount { get; set; }
    }

    public partial class Model
    {
        [JsonProperty("itemid")]
        public long Itemid { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("current_promotion_reserved_stock")]
        public long CurrentPromotionReservedStock { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("model_sku")]
        public string ModelSku { get; set; }

        [JsonProperty("promotionid")]
        public long Promotionid { get; set; }

        [JsonProperty("price")]
        public long? Price { get; set; }

        [JsonProperty("price_stocks")]
        public PriceStock[] PriceStocks { get; set; }

        [JsonProperty("current_promotion_has_reserve_stock")]
        public bool CurrentPromotionHasReserveStock { get; set; }

        [JsonProperty("normal_stock")]
        public long NormalStock { get; set; }

        [JsonProperty("extinfo")]
        public Extinfo Extinfo { get; set; }

        [JsonProperty("price_before_discount")]
        public long? PriceBeforeDiscount { get; set; }

        [JsonProperty("modelid")]
        public long Modelid { get; set; }

        [JsonProperty("stock")]
        public long? Stock { get; set; }

        [JsonProperty("has_gimmick_tag")]
        public bool HasGimmickTag { get; set; }

        [JsonProperty("key_measurement")]
        public object KeyMeasurement { get; set; }
    }

    public partial class Extinfo
    {
        [JsonProperty("tier_index")]
        public long[] TierIndex { get; set; }

        [JsonProperty("group_buy_info")]
        public object GroupBuyInfo { get; set; }

        [JsonProperty("is_pre_order")]
        public bool IsPreOrder { get; set; }

        [JsonProperty("estimated_days")]
        public long EstimatedDays { get; set; }
    }

    public partial class PriceStock
    {
        [JsonProperty("allocated_stock")]
        public long? AllocatedStock { get; set; }

        [JsonProperty("stock_breakdown_by_location")]
        public StockBreakdownByLocation[] StockBreakdownByLocation { get; set; }
    }

    public partial class StockBreakdownByLocation
    {
        [JsonProperty("location_id")]
        public LocationId LocationId { get; set; }

        [JsonProperty("available_stock")]
        public long? AvailableStock { get; set; }

        [JsonProperty("fulfilment_type")]
        public long? FulfilmentType { get; set; }

        [JsonProperty("address_id")]
        public object AddressId { get; set; }
    }

    public partial class OverallPurchaseLimit
    {
        [JsonProperty("order_max_purchase_limit")]
        public long? OrderMaxPurchaseLimit { get; set; }

        [JsonProperty("overall_purchase_limit")]
        public object OverallPurchaseLimitOverallPurchaseLimit { get; set; }

        [JsonProperty("item_overall_quota")]
        public object ItemOverallQuota { get; set; }

        [JsonProperty("start_date")]
        public object StartDate { get; set; }

        [JsonProperty("end_date")]
        public object EndDate { get; set; }
    }

    public partial class ShopVoucher
    {
        [JsonProperty("promotionid")]
        public long Promotionid { get; set; }

        [JsonProperty("voucher_code")]
        public string VoucherCode { get; set; }

        [JsonProperty("signature")]
        public string Signature { get; set; }

        [JsonProperty("use_type")]
        public object UseType { get; set; }

        [JsonProperty("platform_type")]
        public object PlatformType { get; set; }

        [JsonProperty("voucher_market_type")]
        public object VoucherMarketType { get; set; }

        [JsonProperty("min_spend")]
        public long MinSpend { get; set; }

        [JsonProperty("used_price")]
        public object UsedPrice { get; set; }

        [JsonProperty("current_spend")]
        public object CurrentSpend { get; set; }

        [JsonProperty("product_limit")]
        public bool ProductLimit { get; set; }

        [JsonProperty("quota_type")]
        public long QuotaType { get; set; }

        [JsonProperty("percentage_claimed")]
        public long PercentageClaimed { get; set; }

        [JsonProperty("percentage_used")]
        public long PercentageUsed { get; set; }

        [JsonProperty("start_time")]
        public long StartTime { get; set; }

        [JsonProperty("end_time")]
        public long EndTime { get; set; }

        [JsonProperty("collect_time")]
        public object CollectTime { get; set; }

        [JsonProperty("claim_start_time")]
        public object ClaimStartTime { get; set; }

        [JsonProperty("valid_days")]
        public object ValidDays { get; set; }

        [JsonProperty("reward_type")]
        public long RewardType { get; set; }

        [JsonProperty("reward_percentage")]
        public object RewardPercentage { get; set; }

        [JsonProperty("reward_value")]
        public object RewardValue { get; set; }

        [JsonProperty("reward_cap")]
        public object RewardCap { get; set; }

        [JsonProperty("coin_earned")]
        public object CoinEarned { get; set; }

        [JsonProperty("title")]
        public object Title { get; set; }

        [JsonProperty("use_link")]
        public object UseLink { get; set; }

        [JsonProperty("icon_hash")]
        public string IconHash { get; set; }

        [JsonProperty("icon_text")]
        public string IconText { get; set; }

        [JsonProperty("icon_url")]
        public object IconUrl { get; set; }

        [JsonProperty("customised_labels")]
        public object[] CustomisedLabels { get; set; }

        [JsonProperty("customised_product_scope_tags")]
        public object CustomisedProductScopeTags { get; set; }

        [JsonProperty("shop_id")]
        public long ShopId { get; set; }

        [JsonProperty("shop_name")]
        public object ShopName { get; set; }

        [JsonProperty("is_shop_preferred")]
        public bool IsShopPreferred { get; set; }

        [JsonProperty("is_shop_official")]
        public bool IsShopOfficial { get; set; }

        [JsonProperty("shop_count")]
        public object ShopCount { get; set; }

        [JsonProperty("ui_display_type")]
        public object UiDisplayType { get; set; }

        [JsonProperty("customised_mall_name")]
        public object CustomisedMallName { get; set; }

        [JsonProperty("small_icon_list")]
        public object SmallIconList { get; set; }

        [JsonProperty("dp_category_name")]
        public object DpCategoryName { get; set; }

        [JsonProperty("invalid_message_code")]
        public object InvalidMessageCode { get; set; }

        [JsonProperty("invalid_message")]
        public object InvalidMessage { get; set; }

        [JsonProperty("display_labels")]
        public object DisplayLabels { get; set; }

        [JsonProperty("wallet_redeemable")]
        public object WalletRedeemable { get; set; }

        [JsonProperty("customer_reference_id")]
        public object CustomerReferenceId { get; set; }

        [JsonProperty("fully_redeemed")]
        public object FullyRedeemed { get; set; }

        [JsonProperty("has_expired")]
        public object HasExpired { get; set; }

        [JsonProperty("disabled")]
        public object Disabled { get; set; }

        [JsonProperty("voucher_external_market_type")]
        public object VoucherExternalMarketType { get; set; }

        [JsonProperty("now_food_extra_info")]
        public object NowFoodExtraInfo { get; set; }

        [JsonProperty("airpay_opv_extra_info")]
        public object AirpayOpvExtraInfo { get; set; }

        [JsonProperty("partner_extra_info")]
        public object PartnerExtraInfo { get; set; }

        [JsonProperty("discount_value")]
        public long DiscountValue { get; set; }

        [JsonProperty("discount_percentage")]
        public long DiscountPercentage { get; set; }

        [JsonProperty("discount_cap")]
        public long DiscountCap { get; set; }

        [JsonProperty("coin_percentage")]
        public object CoinPercentage { get; set; }

        [JsonProperty("coin_cap")]
        public object CoinCap { get; set; }

        [JsonProperty("usage_limit")]
        public object UsageLimit { get; set; }

        [JsonProperty("used_count")]
        public object UsedCount { get; set; }

        [JsonProperty("left_count")]
        public object LeftCount { get; set; }

        [JsonProperty("shopee_wallet_only")]
        public object ShopeeWalletOnly { get; set; }

        [JsonProperty("new_user_only")]
        public object NewUserOnly { get; set; }

        [JsonProperty("description")]
        public object Description { get; set; }

        [JsonProperty("shop_logo")]
        public object ShopLogo { get; set; }

        [JsonProperty("error_code")]
        public long ErrorCode { get; set; }

        [JsonProperty("is_claimed_before")]
        public bool IsClaimedBefore { get; set; }
    }

    public partial class SplInfo
    {
        [JsonProperty("installment_info")]
        public object InstallmentInfo { get; set; }

        [JsonProperty("user_credit_info")]
        public object UserCreditInfo { get; set; }

        [JsonProperty("channel_id")]
        public object ChannelId { get; set; }

        [JsonProperty("show_spl")]
        public bool ShowSpl { get; set; }

        [JsonProperty("show_spl_lite")]
        public bool ShowSplLite { get; set; }
    }

    public partial class TierVariation
    {
        //[JsonProperty("name")]
        //public string Name { get; set; }

        //[JsonProperty("options")]
        //public string[] Options { get; set; }

        //[JsonProperty("images")]
        //public string[] Images { get; set; }

        //[JsonProperty("properties")]
        //public object Properties { get; set; }

        //[JsonProperty("type")]
        //public long Type { get; set; }

        [JsonProperty("summed_stocks")]
        public object SummedStocks { get; set; }
    }

    public partial class VideoInfoList
    {
        [JsonProperty("video_id")]
        public string VideoId { get; set; }

        [JsonProperty("thumb_url")]
        public string ThumbUrl { get; set; }

        [JsonProperty("duration")]
        public long Duration { get; set; }

        [JsonProperty("version")]
        public long Version { get; set; }

        [JsonProperty("vid")]
        public string Vid { get; set; }

        [JsonProperty("formats")]
        public object[] Formats { get; set; }

        [JsonProperty("default_format")]
        public DefaultFormat DefaultFormat { get; set; }
    }

    public partial class DefaultFormat
    {
        [JsonProperty("format")]
        public long Format { get; set; }

        [JsonProperty("defn")]
        public string Defn { get; set; }

        [JsonProperty("profile")]
        public string Profile { get; set; }

        [JsonProperty("path")]
        public string Path { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("width")]
        public long? Width { get; set; }

        [JsonProperty("height")]
        public long? Height { get; set; }
    }

    public enum LocationId { Vnz };

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            { 
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
     
}
