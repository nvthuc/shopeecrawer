﻿using System;
using System.Collections.Generic;

using System.Globalization;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ShopeeCrawer.Model
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse);

    public class ProductListData
    {
        [JsonProperty("total_count")]
        public int TotalCount { get; set; }
        
        [JsonProperty("nomore")]
        public bool NoMore { get; set; }
        
        [JsonProperty("items")]
        public List<Item> Items { get; set; } 
    }

 
    public partial class Item
    {
        [JsonProperty("item_basic")]
        public ItemBasic ItemBasic { get; set; }

        [JsonProperty("adsid")]
        public object Adsid { get; set; }

        [JsonProperty("campaignid")]
        public object Campaignid { get; set; }

        [JsonProperty("distance")]
        public object Distance { get; set; }

        [JsonProperty("match_type")]
        public object MatchType { get; set; }

        [JsonProperty("ads_keyword")]
        public object AdsKeyword { get; set; }

        [JsonProperty("deduction_info")]
        public object DeductionInfo { get; set; }

        [JsonProperty("collection_id")]
        public object CollectionId { get; set; }

        [JsonProperty("display_name")]
        public object DisplayName { get; set; }

        [JsonProperty("campaign_stock")]
        public object CampaignStock { get; set; }

        [JsonProperty("json_data")]
        public string JsonData { get; set; }

        [JsonProperty("tracking_info")]
        public TrackingInfo TrackingInfo { get; set; }

        [JsonProperty("itemid")]
        public long Itemid { get; set; }

        [JsonProperty("shopid")]
        public long Shopid { get; set; }

        [JsonProperty("algo_image")]
        public object AlgoImage { get; set; }

        [JsonProperty("fe_flags")]
        public object FeFlags { get; set; }

        [JsonProperty("item_type")]
        public long ItemType { get; set; }

        [JsonProperty("foody_item")]
        public object FoodyItem { get; set; }

        [JsonProperty("personalized_labels")]
        public object PersonalizedLabels { get; set; }

        [JsonProperty("biz_json")]
        public object BizJson { get; set; }
    }

    public partial class ItemBasic
    {
        [JsonProperty("itemid")]
        public long Itemid { get; set; }

        [JsonProperty("shopid")]
        public long Shopid { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("images")]
        public string[] Images { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("stock")]
        public long Stock { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("ctime")]
        public long Ctime { get; set; }

        [JsonProperty("sold")]
        public long Sold { get; set; }

        [JsonProperty("historical_sold")]
        public long HistoricalSold { get; set; }

        [JsonProperty("liked")]
        public bool Liked { get; set; }

        [JsonProperty("liked_count")]
        public long LikedCount { get; set; }

        [JsonProperty("view_count")]
        public object ViewCount { get; set; }

        [JsonProperty("catid")]
        public long Catid { get; set; }

        [JsonProperty("brand")]
        public string Brand { get; set; }

        [JsonProperty("cmt_count")]
        public long CmtCount { get; set; }

        [JsonProperty("flag")]
        public long Flag { get; set; }

        [JsonProperty("cb_option")]
        public long CbOption { get; set; }

        [JsonProperty("item_status")]
        public string ItemStatus { get; set; }

        [JsonProperty("price")]
        public long Price { get; set; }

        [JsonProperty("price_min")]
        public long PriceMin { get; set; }

        [JsonProperty("price_max")]
        public long PriceMax { get; set; }

        [JsonProperty("price_min_before_discount")]
        public long PriceMinBeforeDiscount { get; set; }

        [JsonProperty("price_max_before_discount")]
        public long PriceMaxBeforeDiscount { get; set; }

        [JsonProperty("hidden_price_display")]
        public object HiddenPriceDisplay { get; set; }

        [JsonProperty("price_before_discount")]
        public long PriceBeforeDiscount { get; set; }

        [JsonProperty("has_lowest_price_guarantee")]
        public bool HasLowestPriceGuarantee { get; set; }

        [JsonProperty("show_discount")]
        public long ShowDiscount { get; set; }

        [JsonProperty("raw_discount")]
        public long RawDiscount { get; set; }

        [JsonProperty("discount")]
        public string Discount { get; set; }

        [JsonProperty("is_category_failed")]
        public object IsCategoryFailed { get; set; }

        [JsonProperty("size_chart")]
        public string SizeChart { get; set; }

        [JsonProperty("video_info_list")]
        public object VideoInfoList { get; set; }

        [JsonProperty("tier_variations")]
        public TierVariation[] TierVariations { get; set; }

        [JsonProperty("item_type")]
        public long ItemType { get; set; }

        [JsonProperty("reference_item_id")]
        public string ReferenceItemId { get; set; }

        [JsonProperty("transparent_background_image")]
        public string TransparentBackgroundImage { get; set; }

        [JsonProperty("is_adult")]
        public bool IsAdult { get; set; }

        [JsonProperty("badge_icon_type")]
        public long BadgeIconType { get; set; }

        [JsonProperty("shopee_verified")]
        public bool ShopeeVerified { get; set; }

        [JsonProperty("is_official_shop")]
        public bool IsOfficialShop { get; set; }

        [JsonProperty("show_official_shop_label")]
        public bool ShowOfficialShopLabel { get; set; }

        [JsonProperty("show_shopee_verified_label")]
        public bool ShowShopeeVerifiedLabel { get; set; }

        [JsonProperty("show_official_shop_label_in_title")]
        public bool ShowOfficialShopLabelInTitle { get; set; }

        [JsonProperty("is_cc_installment_payment_eligible")]
        public bool IsCcInstallmentPaymentEligible { get; set; }

        [JsonProperty("is_non_cc_installment_payment_eligible")]
        public bool IsNonCcInstallmentPaymentEligible { get; set; }

        [JsonProperty("coin_earn_label")]
        public object CoinEarnLabel { get; set; }

        [JsonProperty("show_free_shipping")]
        public bool ShowFreeShipping { get; set; }

        [JsonProperty("preview_info")]
        public object PreviewInfo { get; set; }

        [JsonProperty("coin_info")]
        public object CoinInfo { get; set; }

        [JsonProperty("exclusive_price_info")]
        public object ExclusivePriceInfo { get; set; }

        [JsonProperty("bundle_deal_id")]
        public long BundleDealId { get; set; }

        [JsonProperty("can_use_bundle_deal")]
        public bool CanUseBundleDeal { get; set; }

        [JsonProperty("bundle_deal_info")]
        public BundleDealInfo BundleDealInfo { get; set; }

        [JsonProperty("is_group_buy_item")]
        public object IsGroupBuyItem { get; set; }

        [JsonProperty("has_group_buy_stock")]
        public object HasGroupBuyStock { get; set; }

        [JsonProperty("group_buy_info")]
        public object GroupBuyInfo { get; set; }

        [JsonProperty("welcome_package_type")]
        public long WelcomePackageType { get; set; }

        [JsonProperty("welcome_package_info")]
        public object WelcomePackageInfo { get; set; }

        [JsonProperty("add_on_deal_info")]
        public object AddOnDealInfo { get; set; }

        [JsonProperty("can_use_wholesale")]
        public bool CanUseWholesale { get; set; }

        [JsonProperty("is_preferred_plus_seller")]
        public bool IsPreferredPlusSeller { get; set; }

        [JsonProperty("shop_location")]
        public string ShopLocation { get; set; }

        [JsonProperty("has_model_with_available_shopee_stock")]
        public bool HasModelWithAvailableShopeeStock { get; set; }

        [JsonProperty("voucher_info")]
        public VoucherInfo VoucherInfo { get; set; }

        [JsonProperty("can_use_cod")]
        public bool CanUseCod { get; set; }

        [JsonProperty("is_on_flash_sale")]
        public bool IsOnFlashSale { get; set; }

        [JsonProperty("spl_installment_tenure")]
        public object SplInstallmentTenure { get; set; }

        [JsonProperty("is_live_streaming_price")]
        public object IsLiveStreamingPrice { get; set; }

        [JsonProperty("is_mart")]
        public bool IsMart { get; set; }

        [JsonProperty("pack_size")]
        public object PackSize { get; set; }

        [JsonProperty("deep_discount_skin")]
        public object DeepDiscountSkin { get; set; }

        [JsonProperty("is_service_by_shopee")]
        public bool IsServiceByShopee { get; set; }
    }

    public partial class BundleDealInfo
    {
        [JsonProperty("bundle_deal_id")]
        public long BundleDealId { get; set; }

        [JsonProperty("bundle_deal_label")]
        public string BundleDealLabel { get; set; }
    }

    public partial class TierVariation
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("options")]
        public string[] Options { get; set; }

        [JsonProperty("images")]
        public string[] Images { get; set; }

        [JsonProperty("properties")]
        public object[] Properties { get; set; }

        [JsonProperty("type")]
        public long Type { get; set; }
    }

    public partial class VoucherInfo
    {
        [JsonProperty("promotion_id")]
        public long PromotionId { get; set; }

        [JsonProperty("voucher_code")]
        public string VoucherCode { get; set; }

        [JsonProperty("label")]
        public string Label { get; set; }
    }

    public partial class TrackingInfo
    {
        [JsonProperty("viral_spu_tracking")]
        public object ViralSpuTracking { get; set; }

        [JsonProperty("business_tracking")]
        public object BusinessTracking { get; set; }

        [JsonProperty("multi_search_tracking")]
        public object MultiSearchTracking { get; set; }

        [JsonProperty("groupid")]
        public long Groupid { get; set; }

        [JsonProperty("ruleid")]
        public long[] Ruleid { get; set; }



    }
}
