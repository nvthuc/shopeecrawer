﻿using System.Collections.Generic;

namespace ShopeeCrawer.Model
{
    class ProductDetailViewModel
    {
        /// <summary> 
        ///  ID danh mục
        /// Vui lòng tham khảo Danh sách danh mục để tìm ID danh mục chính xác của sản phẩm (Xem tại Kênh Người bán > Sản Phẩm > Nhập Hàng Loạt > Thêm Sản Phẩm Mới).
        /// </summary>
        public string ps_category_list_id { get; set; }
        /// <summary> 
        ///  Tên sản phẩm
        /// Nhập vào tên sản phẩm (tối đa 120 ký tự).
        /// </summary>
        public string ps_product_name { get; set; }
        /// <summary> 
        ///  Mô tả sản phẩm
        /// Mô tả sản phẩm của bạn (yêu cầu từ 100-3000 ký tự)
        /// </summary>
        public string ps_product_description { get; set; }
        /// <summary> 
        ///  Giá
        /// Cột này là bắt buộc cho sản phẩm không có phân loại hàng. Nếu sản phẩm có nhiều phân loại hàng, bạn có thể bỏ trống cột này. Khoảng giá cho phép: 1.000 /// - 30.000.000
        /// </summary>
        public decimal? ps_price { get; set; }
        /// <summary> 
        ///  Kho
        /// Cột này là bắt buộc nếu sản phẩm của bạn không chứa bất kỳ phân loại hàng nào.  Nếu sản phẩm có nhiều phân loại hàng, bạn có thể bỏ trống cột này. Số lượng cho phép: 1 /// - 999999
        /// </summary>
        public decimal ps_stock { get; set; }
        /// <summary> 
        ///  Khối lượng sản phẩm
        /// (Bắt buộc) Nhập khối lượng sản phẩm của bạn bằng đơn vị gram (g). Ứng dụng sẽ tự động tính phí vận chuyển tương ứng với các Kênh vận chuyển bạn đã kích hoạt.

        /// Lưu ý:
        /// - Khối lượng tối thiểu: 50g
        /// - Khối lượng tối đa: 10.000g (GHTK), 50.000g (GHN và VTP)
        /// </summary>
        public decimal ps_product_weight { get; set; }
        /// <summary> 
        ///  Chuẩn bị hàng
        /// Nhập vào số ngày bạn cần cho việc chuẩn bị hàng. Nếu bỏ trống, số ngày sẽ được mặc định là 2.
        /// Lưu ý:
        /// - Đối với hàng có sẵn, thời gian giao hàng quy định là 2 ngày (không thể thay đổi)
        /// - Đối với hàng không có sẵn, bạn phải chọn từ 7 ngày trở lên (tối đa 30 ngày)
        /// </summary>
        public int ps_days_to_ship { get; set; }
        /// <summary> 
        ///  Mã Sản Phẩm
        /// (Tùy chọn) Nhập Mã sản phẩm của từng sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_sku_ref_no_parent { get; set; }
        /// <summary> 
        ///   Nếu sản phẩm không có bất cứ phân loại hàng nào (ví dụ: kích thước, màu sắc)  Bạn có thể tiến hành qua sản phẩm tiếp theo  Nếu sản phẩm của bạn có phân loại hàng, vui lòng tiếp tục để hoàn thành chi tiết. Mỗi sản phẩm chỉ có tối đa 20 phân loại hàng'
        /// Nếu sản phẩm không có bất cứ phân loại hàng nào (ví dụ: kích thước, màu sắc.v.v...). Bạn có thể tiến hành qua sản phẩm tiếp theo. Nếu sản phẩm của bạn có phân loại hàng, vui lòng tiếp tục để hoàn thành chi tiết. Mỗi sản phẩm chỉ có tối đa 20 phân loại hàng.
        /// </summary>
        public string ps_mass_upload_variation_help { get; set; }
        /// <summary> 
        ///  Loại hàng 1: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_1_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 1: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_1_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 1: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_1_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 1: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_1_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 2: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_2_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 2: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_2_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 2: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_2_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 2: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_2_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 3: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_3_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 3: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_3_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 3: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_3_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 3: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_3_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 4: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_4_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 4: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_4_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 4: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_4_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 4: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_4_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 5: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_5_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 5: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_5_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 5: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_5_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 5: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_5_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 6: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_6_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 6: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_6_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 6: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_6_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 6: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_6_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 7: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_7_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 7: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_7_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 7: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_7_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 7: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_7_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 8: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_8_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 8: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_8_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 8: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_8_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 8: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_8_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 9: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_9_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 9: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_9_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 9: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_9_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 9: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_9_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 10: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_10_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 10: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_10_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 10: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_10_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 10: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_10_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 11: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_11_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 11: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_11_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 11: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_11_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 11: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_11_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 12: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_12_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 12: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_12_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 12: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_12_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 12: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_12_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 13: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_13_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 13: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_13_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 13: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_13_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 13: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_13_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 14: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_14_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 14: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_14_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 14: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_14_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 14: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_14_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 15: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_15_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 15: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_15_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 15: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_15_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 15: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_15_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 16: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_16_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 16: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_16_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 16: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_16_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 16: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_16_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 17: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_17_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 17: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_17_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 17: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_17_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 17: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_17_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 18: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_18_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 18: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_18_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 18: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_18_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 18: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_18_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 19: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_19_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 19: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_19_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 19: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_19_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 19: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_19_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Loại hàng 20: Mã phân loại hàng
        /// (Tùy chọn) Nhập Mã phân loại hàng của từng phân loại sản phẩm (nhiều nhất 100 kí tự)
        /// </summary>
        public string ps_variation_20_ps_variation_sku { get; set; }
        /// <summary> 
        ///  Loại hàng 20: Tên loại hàng
        /// Nhập vào tên phân loại hàng (tối đa 20 ký tự).
        /// </summary>
        public string ps_variation_20_ps_variation_name { get; set; }
        /// <summary> 
        ///  Loại hàng 20: Giá
        /// Nhập vào giá phân loại hàng (Khoảng giá cho phép: 1.000 /// - 30.000.000).
        /// </summary>
        public decimal ps_variation_20_ps_variation_price { get; set; }
        /// <summary> 
        ///  Loại hàng 20: Kho
        /// Nhập vào kho của phân loại hàng (Số lượng cho phép: 1 /// - 999999).
        /// </summary>
        public decimal ps_variation_20_ps_variation_stock { get; set; }
        /// <summary> 
        ///  Ảnh 1
        /// Nhập đường dẫn đến hình ảnh (đây sẽ là ảnh đại diện)
        /// </summary>
        public string ps_img_1 { get; set; }
        /// <summary> 
        ///  Ảnh 2
        /// Nhập đường dẫn đến hình ảnh
        /// </summary>
        public string ps_img_2 { get; set; }
        /// <summary> 
        ///  Ảnh 3
        /// Nhập đường dẫn đến hình ảnh
        /// </summary>
        public string ps_img_3 { get; set; }
        /// <summary> 
        ///  Ảnh 4
        /// Nhập đường dẫn đến hình ảnh
        /// </summary>
        public string ps_img_4 { get; set; }
        /// <summary> 
        ///  Ảnh 5
        /// Nhập đường dẫn đến hình ảnh
        /// </summary>
        public string ps_img_5 { get; set; }
        /// <summary> 
        ///  Ảnh 6
        /// Nhập đường dẫn đến hình ảnh
        /// </summary>
        public string ps_img_6 { get; set; }
        /// <summary> 
        ///  Ảnh 7
        /// Nhập đường dẫn đến hình ảnh
        /// </summary>
        public string ps_img_7 { get; set; }
        /// <summary> 
        ///  Ảnh 8
        /// Nhập đường dẫn đến hình ảnh
        /// </summary>
        public string ps_img_8 { get; set; }
        /// <summary> 
        ///  Ảnh 9
        /// Nhập đường dẫn đến hình ảnh
        /// </summary>
        public string ps_img_9 { get; set; }
        /// <summary> 
        ///  /// 
        /// (Optional) Please set shipment info for your products; If left empty, the default logistics info from your last product settings will be used. Please be noted that the logistics info can be uploaded successfully only if you have selected that logistics channel_at shop settings.
        /// </summary>
        public string ps_mass_upload_shipment_help { get; set; }
        /// <summary> 
        ///  Viettel Post
        /// Enter ""Mở"" or ""Tắt"" to enable or disable Viettel Post option for your respective products. If left blank, the default logistics settings from your latest published products will be used. Shipping fee will be calculated based on product weight.
        /// </summary>
        public string channel_50010_switch { get; set; }
        /// <summary> 
        ///  Giao Hàng Nhanh
        /// Enter ""Mở"" or ""Tắt"" to enable or disable Giao Hàng Nhanh option for your respective products. If left blank, the default logistics settings from your latest published products will be used. Shipping fee will be calculated based on product weight.
        /// </summary>
        public string channel_50011_switch { get; set; }
        /// <summary> 
        ///  Giao Hàng Tiết Kiệm
        /// Enter ""Mở"" or ""Tắt"" to enable or disable Giao Hàng Tiết Kiệm option for your respective products. If left blank, the default logistics settings from your latest published products will be used. Shipping fee will be calculated based on product weight.
        /// </summary>
        public string channel_50012_switch { get; set; }
        /// <summary> 
        ///  Shopee 4H
        /// Enter ""Mở"" or ""Tắt"" to enable or disable Giao Hàng Tiết Kiệm 2H option for your respective products. If left blank, the default logistics settings from your latest published products will be used. Shipping fee will be calculated based on product weight.
        /// </summary>
        public string channel_50066_switch { get; set; }

        /// <summary>
        /// ID ảnh tải về
        /// </summary>
        public List<string> ImageIds { get; set; }

    }
}
