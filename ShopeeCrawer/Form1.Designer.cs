﻿namespace ShopeeCrawer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.repositoryItemImageEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.grcData = new DevExpress.XtraGrid.GridControl();
            this.bdsData = new System.Windows.Forms.BindingSource(this.components);
            this.grvData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colps_category_list_id = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_product_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_product_description = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_product_weight = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_days_to_ship = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_sku_ref_no_parent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_mass_upload_variation_help = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_1_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_1_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_1_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_1_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_2_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_2_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_2_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_2_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_3_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_3_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_3_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_3_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_4_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_4_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_4_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_4_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_5_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_5_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_5_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_5_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_6_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_6_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_6_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_6_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_7_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_7_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_7_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_7_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_8_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_8_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_8_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_8_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_9_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_9_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_9_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_9_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_10_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_10_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_10_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_10_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_11_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_11_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_11_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_11_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_12_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_12_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_12_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_12_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_13_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_13_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_13_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_13_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_14_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_14_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_14_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_14_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_15_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_15_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_15_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_15_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_16_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_16_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_16_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_16_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_17_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_17_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_17_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_17_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_18_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_18_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_18_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_18_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_19_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_19_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_19_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_19_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_20_ps_variation_sku = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_20_ps_variation_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_20_ps_variation_price = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_variation_20_ps_variation_stock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_img_1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_img_2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_img_3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_img_4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_img_5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_img_6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_img_7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_img_8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_img_9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colps_mass_upload_shipment_help = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colchannel_50010_switch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colchannel_50011_switch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colchannel_50012_switch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colchannel_50066_switch = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.btnGetdata = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtShopId = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::ShopeeCrawer.WaitForm1), true, true);
            this.txtFromPages = new System.Windows.Forms.NumericUpDown();
            this.txtLimit = new System.Windows.Forms.NumericUpDown();
            this.txtToPage = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.txtShopName = new DevExpress.XtraEditors.TextEdit();
            this.txtBy = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtOrder = new DevExpress.XtraEditors.ComboBoxEdit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromPages)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLimit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShopName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBy.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrder.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // repositoryItemImageEdit2
            // 
            this.repositoryItemImageEdit2.AutoHeight = false;
            this.repositoryItemImageEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit2.Name = "repositoryItemImageEdit2";
            // 
            // grcData
            // 
            this.grcData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grcData.DataSource = this.bdsData;
            this.grcData.Location = new System.Drawing.Point(12, 64);
            this.grcData.MainView = this.grvData;
            this.grcData.Name = "grcData";
            this.grcData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageEdit1,
            this.repositoryItemImageEdit2});
            this.grcData.Size = new System.Drawing.Size(776, 374);
            this.grcData.TabIndex = 4;
            this.grcData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvData});
            // 
            // bdsData
            // 
            this.bdsData.DataSource = typeof(ShopeeCrawer.Model.ProductDetailViewModel);
            // 
            // grvData
            // 
            this.grvData.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colps_category_list_id,
            this.colps_product_name,
            this.colps_product_description,
            this.colps_price,
            this.colps_stock,
            this.colps_product_weight,
            this.colps_days_to_ship,
            this.colps_sku_ref_no_parent,
            this.colps_mass_upload_variation_help,
            this.colps_variation_1_ps_variation_sku,
            this.colps_variation_1_ps_variation_name,
            this.colps_variation_1_ps_variation_price,
            this.colps_variation_1_ps_variation_stock,
            this.colps_variation_2_ps_variation_sku,
            this.colps_variation_2_ps_variation_name,
            this.colps_variation_2_ps_variation_price,
            this.colps_variation_2_ps_variation_stock,
            this.colps_variation_3_ps_variation_sku,
            this.colps_variation_3_ps_variation_name,
            this.colps_variation_3_ps_variation_price,
            this.colps_variation_3_ps_variation_stock,
            this.colps_variation_4_ps_variation_sku,
            this.colps_variation_4_ps_variation_name,
            this.colps_variation_4_ps_variation_price,
            this.colps_variation_4_ps_variation_stock,
            this.colps_variation_5_ps_variation_sku,
            this.colps_variation_5_ps_variation_name,
            this.colps_variation_5_ps_variation_price,
            this.colps_variation_5_ps_variation_stock,
            this.colps_variation_6_ps_variation_sku,
            this.colps_variation_6_ps_variation_name,
            this.colps_variation_6_ps_variation_price,
            this.colps_variation_6_ps_variation_stock,
            this.colps_variation_7_ps_variation_sku,
            this.colps_variation_7_ps_variation_name,
            this.colps_variation_7_ps_variation_price,
            this.colps_variation_7_ps_variation_stock,
            this.colps_variation_8_ps_variation_sku,
            this.colps_variation_8_ps_variation_name,
            this.colps_variation_8_ps_variation_price,
            this.colps_variation_8_ps_variation_stock,
            this.colps_variation_9_ps_variation_sku,
            this.colps_variation_9_ps_variation_name,
            this.colps_variation_9_ps_variation_price,
            this.colps_variation_9_ps_variation_stock,
            this.colps_variation_10_ps_variation_sku,
            this.colps_variation_10_ps_variation_name,
            this.colps_variation_10_ps_variation_price,
            this.colps_variation_10_ps_variation_stock,
            this.colps_variation_11_ps_variation_sku,
            this.colps_variation_11_ps_variation_name,
            this.colps_variation_11_ps_variation_price,
            this.colps_variation_11_ps_variation_stock,
            this.colps_variation_12_ps_variation_sku,
            this.colps_variation_12_ps_variation_name,
            this.colps_variation_12_ps_variation_price,
            this.colps_variation_12_ps_variation_stock,
            this.colps_variation_13_ps_variation_sku,
            this.colps_variation_13_ps_variation_name,
            this.colps_variation_13_ps_variation_price,
            this.colps_variation_13_ps_variation_stock,
            this.colps_variation_14_ps_variation_sku,
            this.colps_variation_14_ps_variation_name,
            this.colps_variation_14_ps_variation_price,
            this.colps_variation_14_ps_variation_stock,
            this.colps_variation_15_ps_variation_sku,
            this.colps_variation_15_ps_variation_name,
            this.colps_variation_15_ps_variation_price,
            this.colps_variation_15_ps_variation_stock,
            this.colps_variation_16_ps_variation_sku,
            this.colps_variation_16_ps_variation_name,
            this.colps_variation_16_ps_variation_price,
            this.colps_variation_16_ps_variation_stock,
            this.colps_variation_17_ps_variation_sku,
            this.colps_variation_17_ps_variation_name,
            this.colps_variation_17_ps_variation_price,
            this.colps_variation_17_ps_variation_stock,
            this.colps_variation_18_ps_variation_sku,
            this.colps_variation_18_ps_variation_name,
            this.colps_variation_18_ps_variation_price,
            this.colps_variation_18_ps_variation_stock,
            this.colps_variation_19_ps_variation_sku,
            this.colps_variation_19_ps_variation_name,
            this.colps_variation_19_ps_variation_price,
            this.colps_variation_19_ps_variation_stock,
            this.colps_variation_20_ps_variation_sku,
            this.colps_variation_20_ps_variation_name,
            this.colps_variation_20_ps_variation_price,
            this.colps_variation_20_ps_variation_stock,
            this.colps_img_1,
            this.colps_img_2,
            this.colps_img_3,
            this.colps_img_4,
            this.colps_img_5,
            this.colps_img_6,
            this.colps_img_7,
            this.colps_img_8,
            this.colps_img_9,
            this.colps_mass_upload_shipment_help,
            this.colchannel_50010_switch,
            this.colchannel_50011_switch,
            this.colchannel_50012_switch,
            this.colchannel_50066_switch});
            this.grvData.GridControl = this.grcData;
            this.grvData.Name = "grvData";
            this.grvData.OptionsView.ShowFooter = true;
            // 
            // colps_category_list_id
            // 
            this.colps_category_list_id.FieldName = "ps_category_list_id";
            this.colps_category_list_id.MinWidth = 200;
            this.colps_category_list_id.Name = "colps_category_list_id";
            this.colps_category_list_id.Visible = true;
            this.colps_category_list_id.VisibleIndex = 0;
            this.colps_category_list_id.Width = 520;
            // 
            // colps_product_name
            // 
            this.colps_product_name.FieldName = "ps_product_name";
            this.colps_product_name.MinWidth = 200;
            this.colps_product_name.Name = "colps_product_name";
            this.colps_product_name.Visible = true;
            this.colps_product_name.VisibleIndex = 1;
            this.colps_product_name.Width = 500;
            // 
            // colps_product_description
            // 
            this.colps_product_description.FieldName = "ps_product_description";
            this.colps_product_description.MinWidth = 200;
            this.colps_product_description.Name = "colps_product_description";
            this.colps_product_description.Visible = true;
            this.colps_product_description.VisibleIndex = 2;
            this.colps_product_description.Width = 500;
            // 
            // colps_price
            // 
            this.colps_price.FieldName = "ps_price";
            this.colps_price.MinWidth = 200;
            this.colps_price.Name = "colps_price";
            this.colps_price.Visible = true;
            this.colps_price.VisibleIndex = 3;
            this.colps_price.Width = 500;
            // 
            // colps_stock
            // 
            this.colps_stock.FieldName = "ps_stock";
            this.colps_stock.MinWidth = 200;
            this.colps_stock.Name = "colps_stock";
            this.colps_stock.Visible = true;
            this.colps_stock.VisibleIndex = 4;
            this.colps_stock.Width = 500;
            // 
            // colps_product_weight
            // 
            this.colps_product_weight.FieldName = "ps_product_weight";
            this.colps_product_weight.MinWidth = 200;
            this.colps_product_weight.Name = "colps_product_weight";
            this.colps_product_weight.Visible = true;
            this.colps_product_weight.VisibleIndex = 5;
            this.colps_product_weight.Width = 500;
            // 
            // colps_days_to_ship
            // 
            this.colps_days_to_ship.FieldName = "ps_days_to_ship";
            this.colps_days_to_ship.MinWidth = 200;
            this.colps_days_to_ship.Name = "colps_days_to_ship";
            this.colps_days_to_ship.Visible = true;
            this.colps_days_to_ship.VisibleIndex = 6;
            this.colps_days_to_ship.Width = 500;
            // 
            // colps_sku_ref_no_parent
            // 
            this.colps_sku_ref_no_parent.FieldName = "ps_sku_ref_no_parent";
            this.colps_sku_ref_no_parent.MinWidth = 200;
            this.colps_sku_ref_no_parent.Name = "colps_sku_ref_no_parent";
            this.colps_sku_ref_no_parent.Visible = true;
            this.colps_sku_ref_no_parent.VisibleIndex = 7;
            this.colps_sku_ref_no_parent.Width = 500;
            // 
            // colps_mass_upload_variation_help
            // 
            this.colps_mass_upload_variation_help.FieldName = "ps_mass_upload_variation_help";
            this.colps_mass_upload_variation_help.MinWidth = 200;
            this.colps_mass_upload_variation_help.Name = "colps_mass_upload_variation_help";
            this.colps_mass_upload_variation_help.Visible = true;
            this.colps_mass_upload_variation_help.VisibleIndex = 8;
            this.colps_mass_upload_variation_help.Width = 500;
            // 
            // colps_variation_1_ps_variation_sku
            // 
            this.colps_variation_1_ps_variation_sku.FieldName = "ps_variation_1_ps_variation_sku";
            this.colps_variation_1_ps_variation_sku.MinWidth = 200;
            this.colps_variation_1_ps_variation_sku.Name = "colps_variation_1_ps_variation_sku";
            this.colps_variation_1_ps_variation_sku.Visible = true;
            this.colps_variation_1_ps_variation_sku.VisibleIndex = 9;
            this.colps_variation_1_ps_variation_sku.Width = 500;
            // 
            // colps_variation_1_ps_variation_name
            // 
            this.colps_variation_1_ps_variation_name.FieldName = "ps_variation_1_ps_variation_name";
            this.colps_variation_1_ps_variation_name.MinWidth = 200;
            this.colps_variation_1_ps_variation_name.Name = "colps_variation_1_ps_variation_name";
            this.colps_variation_1_ps_variation_name.Visible = true;
            this.colps_variation_1_ps_variation_name.VisibleIndex = 10;
            this.colps_variation_1_ps_variation_name.Width = 500;
            // 
            // colps_variation_1_ps_variation_price
            // 
            this.colps_variation_1_ps_variation_price.FieldName = "ps_variation_1_ps_variation_price";
            this.colps_variation_1_ps_variation_price.MinWidth = 200;
            this.colps_variation_1_ps_variation_price.Name = "colps_variation_1_ps_variation_price";
            this.colps_variation_1_ps_variation_price.Visible = true;
            this.colps_variation_1_ps_variation_price.VisibleIndex = 11;
            this.colps_variation_1_ps_variation_price.Width = 500;
            // 
            // colps_variation_1_ps_variation_stock
            // 
            this.colps_variation_1_ps_variation_stock.FieldName = "ps_variation_1_ps_variation_stock";
            this.colps_variation_1_ps_variation_stock.MinWidth = 200;
            this.colps_variation_1_ps_variation_stock.Name = "colps_variation_1_ps_variation_stock";
            this.colps_variation_1_ps_variation_stock.Visible = true;
            this.colps_variation_1_ps_variation_stock.VisibleIndex = 12;
            this.colps_variation_1_ps_variation_stock.Width = 500;
            // 
            // colps_variation_2_ps_variation_sku
            // 
            this.colps_variation_2_ps_variation_sku.FieldName = "ps_variation_2_ps_variation_sku";
            this.colps_variation_2_ps_variation_sku.MinWidth = 200;
            this.colps_variation_2_ps_variation_sku.Name = "colps_variation_2_ps_variation_sku";
            this.colps_variation_2_ps_variation_sku.Visible = true;
            this.colps_variation_2_ps_variation_sku.VisibleIndex = 13;
            this.colps_variation_2_ps_variation_sku.Width = 500;
            // 
            // colps_variation_2_ps_variation_name
            // 
            this.colps_variation_2_ps_variation_name.FieldName = "ps_variation_2_ps_variation_name";
            this.colps_variation_2_ps_variation_name.MinWidth = 200;
            this.colps_variation_2_ps_variation_name.Name = "colps_variation_2_ps_variation_name";
            this.colps_variation_2_ps_variation_name.Visible = true;
            this.colps_variation_2_ps_variation_name.VisibleIndex = 14;
            this.colps_variation_2_ps_variation_name.Width = 500;
            // 
            // colps_variation_2_ps_variation_price
            // 
            this.colps_variation_2_ps_variation_price.FieldName = "ps_variation_2_ps_variation_price";
            this.colps_variation_2_ps_variation_price.MinWidth = 200;
            this.colps_variation_2_ps_variation_price.Name = "colps_variation_2_ps_variation_price";
            this.colps_variation_2_ps_variation_price.Visible = true;
            this.colps_variation_2_ps_variation_price.VisibleIndex = 15;
            this.colps_variation_2_ps_variation_price.Width = 500;
            // 
            // colps_variation_2_ps_variation_stock
            // 
            this.colps_variation_2_ps_variation_stock.FieldName = "ps_variation_2_ps_variation_stock";
            this.colps_variation_2_ps_variation_stock.MinWidth = 200;
            this.colps_variation_2_ps_variation_stock.Name = "colps_variation_2_ps_variation_stock";
            this.colps_variation_2_ps_variation_stock.Visible = true;
            this.colps_variation_2_ps_variation_stock.VisibleIndex = 16;
            this.colps_variation_2_ps_variation_stock.Width = 500;
            // 
            // colps_variation_3_ps_variation_sku
            // 
            this.colps_variation_3_ps_variation_sku.FieldName = "ps_variation_3_ps_variation_sku";
            this.colps_variation_3_ps_variation_sku.MinWidth = 200;
            this.colps_variation_3_ps_variation_sku.Name = "colps_variation_3_ps_variation_sku";
            this.colps_variation_3_ps_variation_sku.Visible = true;
            this.colps_variation_3_ps_variation_sku.VisibleIndex = 17;
            this.colps_variation_3_ps_variation_sku.Width = 500;
            // 
            // colps_variation_3_ps_variation_name
            // 
            this.colps_variation_3_ps_variation_name.FieldName = "ps_variation_3_ps_variation_name";
            this.colps_variation_3_ps_variation_name.MinWidth = 200;
            this.colps_variation_3_ps_variation_name.Name = "colps_variation_3_ps_variation_name";
            this.colps_variation_3_ps_variation_name.Visible = true;
            this.colps_variation_3_ps_variation_name.VisibleIndex = 18;
            this.colps_variation_3_ps_variation_name.Width = 500;
            // 
            // colps_variation_3_ps_variation_price
            // 
            this.colps_variation_3_ps_variation_price.FieldName = "ps_variation_3_ps_variation_price";
            this.colps_variation_3_ps_variation_price.MinWidth = 200;
            this.colps_variation_3_ps_variation_price.Name = "colps_variation_3_ps_variation_price";
            this.colps_variation_3_ps_variation_price.Visible = true;
            this.colps_variation_3_ps_variation_price.VisibleIndex = 19;
            this.colps_variation_3_ps_variation_price.Width = 500;
            // 
            // colps_variation_3_ps_variation_stock
            // 
            this.colps_variation_3_ps_variation_stock.FieldName = "ps_variation_3_ps_variation_stock";
            this.colps_variation_3_ps_variation_stock.MinWidth = 200;
            this.colps_variation_3_ps_variation_stock.Name = "colps_variation_3_ps_variation_stock";
            this.colps_variation_3_ps_variation_stock.Visible = true;
            this.colps_variation_3_ps_variation_stock.VisibleIndex = 20;
            this.colps_variation_3_ps_variation_stock.Width = 500;
            // 
            // colps_variation_4_ps_variation_sku
            // 
            this.colps_variation_4_ps_variation_sku.FieldName = "ps_variation_4_ps_variation_sku";
            this.colps_variation_4_ps_variation_sku.MinWidth = 200;
            this.colps_variation_4_ps_variation_sku.Name = "colps_variation_4_ps_variation_sku";
            this.colps_variation_4_ps_variation_sku.Visible = true;
            this.colps_variation_4_ps_variation_sku.VisibleIndex = 21;
            this.colps_variation_4_ps_variation_sku.Width = 500;
            // 
            // colps_variation_4_ps_variation_name
            // 
            this.colps_variation_4_ps_variation_name.FieldName = "ps_variation_4_ps_variation_name";
            this.colps_variation_4_ps_variation_name.MinWidth = 200;
            this.colps_variation_4_ps_variation_name.Name = "colps_variation_4_ps_variation_name";
            this.colps_variation_4_ps_variation_name.Visible = true;
            this.colps_variation_4_ps_variation_name.VisibleIndex = 22;
            this.colps_variation_4_ps_variation_name.Width = 500;
            // 
            // colps_variation_4_ps_variation_price
            // 
            this.colps_variation_4_ps_variation_price.FieldName = "ps_variation_4_ps_variation_price";
            this.colps_variation_4_ps_variation_price.MinWidth = 200;
            this.colps_variation_4_ps_variation_price.Name = "colps_variation_4_ps_variation_price";
            this.colps_variation_4_ps_variation_price.Visible = true;
            this.colps_variation_4_ps_variation_price.VisibleIndex = 23;
            this.colps_variation_4_ps_variation_price.Width = 500;
            // 
            // colps_variation_4_ps_variation_stock
            // 
            this.colps_variation_4_ps_variation_stock.FieldName = "ps_variation_4_ps_variation_stock";
            this.colps_variation_4_ps_variation_stock.MinWidth = 200;
            this.colps_variation_4_ps_variation_stock.Name = "colps_variation_4_ps_variation_stock";
            this.colps_variation_4_ps_variation_stock.Visible = true;
            this.colps_variation_4_ps_variation_stock.VisibleIndex = 24;
            this.colps_variation_4_ps_variation_stock.Width = 500;
            // 
            // colps_variation_5_ps_variation_sku
            // 
            this.colps_variation_5_ps_variation_sku.FieldName = "ps_variation_5_ps_variation_sku";
            this.colps_variation_5_ps_variation_sku.MinWidth = 200;
            this.colps_variation_5_ps_variation_sku.Name = "colps_variation_5_ps_variation_sku";
            this.colps_variation_5_ps_variation_sku.Visible = true;
            this.colps_variation_5_ps_variation_sku.VisibleIndex = 25;
            this.colps_variation_5_ps_variation_sku.Width = 500;
            // 
            // colps_variation_5_ps_variation_name
            // 
            this.colps_variation_5_ps_variation_name.FieldName = "ps_variation_5_ps_variation_name";
            this.colps_variation_5_ps_variation_name.MinWidth = 200;
            this.colps_variation_5_ps_variation_name.Name = "colps_variation_5_ps_variation_name";
            this.colps_variation_5_ps_variation_name.Visible = true;
            this.colps_variation_5_ps_variation_name.VisibleIndex = 26;
            this.colps_variation_5_ps_variation_name.Width = 500;
            // 
            // colps_variation_5_ps_variation_price
            // 
            this.colps_variation_5_ps_variation_price.FieldName = "ps_variation_5_ps_variation_price";
            this.colps_variation_5_ps_variation_price.MinWidth = 200;
            this.colps_variation_5_ps_variation_price.Name = "colps_variation_5_ps_variation_price";
            this.colps_variation_5_ps_variation_price.Visible = true;
            this.colps_variation_5_ps_variation_price.VisibleIndex = 27;
            this.colps_variation_5_ps_variation_price.Width = 500;
            // 
            // colps_variation_5_ps_variation_stock
            // 
            this.colps_variation_5_ps_variation_stock.FieldName = "ps_variation_5_ps_variation_stock";
            this.colps_variation_5_ps_variation_stock.MinWidth = 200;
            this.colps_variation_5_ps_variation_stock.Name = "colps_variation_5_ps_variation_stock";
            this.colps_variation_5_ps_variation_stock.Visible = true;
            this.colps_variation_5_ps_variation_stock.VisibleIndex = 28;
            this.colps_variation_5_ps_variation_stock.Width = 500;
            // 
            // colps_variation_6_ps_variation_sku
            // 
            this.colps_variation_6_ps_variation_sku.FieldName = "ps_variation_6_ps_variation_sku";
            this.colps_variation_6_ps_variation_sku.MinWidth = 200;
            this.colps_variation_6_ps_variation_sku.Name = "colps_variation_6_ps_variation_sku";
            this.colps_variation_6_ps_variation_sku.Visible = true;
            this.colps_variation_6_ps_variation_sku.VisibleIndex = 29;
            this.colps_variation_6_ps_variation_sku.Width = 500;
            // 
            // colps_variation_6_ps_variation_name
            // 
            this.colps_variation_6_ps_variation_name.FieldName = "ps_variation_6_ps_variation_name";
            this.colps_variation_6_ps_variation_name.MinWidth = 200;
            this.colps_variation_6_ps_variation_name.Name = "colps_variation_6_ps_variation_name";
            this.colps_variation_6_ps_variation_name.Visible = true;
            this.colps_variation_6_ps_variation_name.VisibleIndex = 30;
            this.colps_variation_6_ps_variation_name.Width = 500;
            // 
            // colps_variation_6_ps_variation_price
            // 
            this.colps_variation_6_ps_variation_price.FieldName = "ps_variation_6_ps_variation_price";
            this.colps_variation_6_ps_variation_price.MinWidth = 200;
            this.colps_variation_6_ps_variation_price.Name = "colps_variation_6_ps_variation_price";
            this.colps_variation_6_ps_variation_price.Visible = true;
            this.colps_variation_6_ps_variation_price.VisibleIndex = 31;
            this.colps_variation_6_ps_variation_price.Width = 500;
            // 
            // colps_variation_6_ps_variation_stock
            // 
            this.colps_variation_6_ps_variation_stock.FieldName = "ps_variation_6_ps_variation_stock";
            this.colps_variation_6_ps_variation_stock.MinWidth = 200;
            this.colps_variation_6_ps_variation_stock.Name = "colps_variation_6_ps_variation_stock";
            this.colps_variation_6_ps_variation_stock.Visible = true;
            this.colps_variation_6_ps_variation_stock.VisibleIndex = 32;
            this.colps_variation_6_ps_variation_stock.Width = 500;
            // 
            // colps_variation_7_ps_variation_sku
            // 
            this.colps_variation_7_ps_variation_sku.FieldName = "ps_variation_7_ps_variation_sku";
            this.colps_variation_7_ps_variation_sku.MinWidth = 200;
            this.colps_variation_7_ps_variation_sku.Name = "colps_variation_7_ps_variation_sku";
            this.colps_variation_7_ps_variation_sku.Visible = true;
            this.colps_variation_7_ps_variation_sku.VisibleIndex = 33;
            this.colps_variation_7_ps_variation_sku.Width = 500;
            // 
            // colps_variation_7_ps_variation_name
            // 
            this.colps_variation_7_ps_variation_name.FieldName = "ps_variation_7_ps_variation_name";
            this.colps_variation_7_ps_variation_name.MinWidth = 200;
            this.colps_variation_7_ps_variation_name.Name = "colps_variation_7_ps_variation_name";
            this.colps_variation_7_ps_variation_name.Visible = true;
            this.colps_variation_7_ps_variation_name.VisibleIndex = 34;
            this.colps_variation_7_ps_variation_name.Width = 500;
            // 
            // colps_variation_7_ps_variation_price
            // 
            this.colps_variation_7_ps_variation_price.FieldName = "ps_variation_7_ps_variation_price";
            this.colps_variation_7_ps_variation_price.MinWidth = 200;
            this.colps_variation_7_ps_variation_price.Name = "colps_variation_7_ps_variation_price";
            this.colps_variation_7_ps_variation_price.Visible = true;
            this.colps_variation_7_ps_variation_price.VisibleIndex = 35;
            this.colps_variation_7_ps_variation_price.Width = 500;
            // 
            // colps_variation_7_ps_variation_stock
            // 
            this.colps_variation_7_ps_variation_stock.FieldName = "ps_variation_7_ps_variation_stock";
            this.colps_variation_7_ps_variation_stock.MinWidth = 200;
            this.colps_variation_7_ps_variation_stock.Name = "colps_variation_7_ps_variation_stock";
            this.colps_variation_7_ps_variation_stock.Visible = true;
            this.colps_variation_7_ps_variation_stock.VisibleIndex = 36;
            this.colps_variation_7_ps_variation_stock.Width = 500;
            // 
            // colps_variation_8_ps_variation_sku
            // 
            this.colps_variation_8_ps_variation_sku.FieldName = "ps_variation_8_ps_variation_sku";
            this.colps_variation_8_ps_variation_sku.MinWidth = 200;
            this.colps_variation_8_ps_variation_sku.Name = "colps_variation_8_ps_variation_sku";
            this.colps_variation_8_ps_variation_sku.Visible = true;
            this.colps_variation_8_ps_variation_sku.VisibleIndex = 37;
            this.colps_variation_8_ps_variation_sku.Width = 500;
            // 
            // colps_variation_8_ps_variation_name
            // 
            this.colps_variation_8_ps_variation_name.FieldName = "ps_variation_8_ps_variation_name";
            this.colps_variation_8_ps_variation_name.MinWidth = 200;
            this.colps_variation_8_ps_variation_name.Name = "colps_variation_8_ps_variation_name";
            this.colps_variation_8_ps_variation_name.Visible = true;
            this.colps_variation_8_ps_variation_name.VisibleIndex = 38;
            this.colps_variation_8_ps_variation_name.Width = 500;
            // 
            // colps_variation_8_ps_variation_price
            // 
            this.colps_variation_8_ps_variation_price.FieldName = "ps_variation_8_ps_variation_price";
            this.colps_variation_8_ps_variation_price.MinWidth = 200;
            this.colps_variation_8_ps_variation_price.Name = "colps_variation_8_ps_variation_price";
            this.colps_variation_8_ps_variation_price.Visible = true;
            this.colps_variation_8_ps_variation_price.VisibleIndex = 39;
            this.colps_variation_8_ps_variation_price.Width = 500;
            // 
            // colps_variation_8_ps_variation_stock
            // 
            this.colps_variation_8_ps_variation_stock.FieldName = "ps_variation_8_ps_variation_stock";
            this.colps_variation_8_ps_variation_stock.MinWidth = 200;
            this.colps_variation_8_ps_variation_stock.Name = "colps_variation_8_ps_variation_stock";
            this.colps_variation_8_ps_variation_stock.Visible = true;
            this.colps_variation_8_ps_variation_stock.VisibleIndex = 40;
            this.colps_variation_8_ps_variation_stock.Width = 500;
            // 
            // colps_variation_9_ps_variation_sku
            // 
            this.colps_variation_9_ps_variation_sku.FieldName = "ps_variation_9_ps_variation_sku";
            this.colps_variation_9_ps_variation_sku.MinWidth = 200;
            this.colps_variation_9_ps_variation_sku.Name = "colps_variation_9_ps_variation_sku";
            this.colps_variation_9_ps_variation_sku.Visible = true;
            this.colps_variation_9_ps_variation_sku.VisibleIndex = 41;
            this.colps_variation_9_ps_variation_sku.Width = 500;
            // 
            // colps_variation_9_ps_variation_name
            // 
            this.colps_variation_9_ps_variation_name.FieldName = "ps_variation_9_ps_variation_name";
            this.colps_variation_9_ps_variation_name.MinWidth = 200;
            this.colps_variation_9_ps_variation_name.Name = "colps_variation_9_ps_variation_name";
            this.colps_variation_9_ps_variation_name.Visible = true;
            this.colps_variation_9_ps_variation_name.VisibleIndex = 42;
            this.colps_variation_9_ps_variation_name.Width = 500;
            // 
            // colps_variation_9_ps_variation_price
            // 
            this.colps_variation_9_ps_variation_price.FieldName = "ps_variation_9_ps_variation_price";
            this.colps_variation_9_ps_variation_price.MinWidth = 200;
            this.colps_variation_9_ps_variation_price.Name = "colps_variation_9_ps_variation_price";
            this.colps_variation_9_ps_variation_price.Visible = true;
            this.colps_variation_9_ps_variation_price.VisibleIndex = 43;
            this.colps_variation_9_ps_variation_price.Width = 500;
            // 
            // colps_variation_9_ps_variation_stock
            // 
            this.colps_variation_9_ps_variation_stock.FieldName = "ps_variation_9_ps_variation_stock";
            this.colps_variation_9_ps_variation_stock.MinWidth = 200;
            this.colps_variation_9_ps_variation_stock.Name = "colps_variation_9_ps_variation_stock";
            this.colps_variation_9_ps_variation_stock.Visible = true;
            this.colps_variation_9_ps_variation_stock.VisibleIndex = 44;
            this.colps_variation_9_ps_variation_stock.Width = 500;
            // 
            // colps_variation_10_ps_variation_sku
            // 
            this.colps_variation_10_ps_variation_sku.FieldName = "ps_variation_10_ps_variation_sku";
            this.colps_variation_10_ps_variation_sku.MinWidth = 200;
            this.colps_variation_10_ps_variation_sku.Name = "colps_variation_10_ps_variation_sku";
            this.colps_variation_10_ps_variation_sku.Visible = true;
            this.colps_variation_10_ps_variation_sku.VisibleIndex = 45;
            this.colps_variation_10_ps_variation_sku.Width = 500;
            // 
            // colps_variation_10_ps_variation_name
            // 
            this.colps_variation_10_ps_variation_name.FieldName = "ps_variation_10_ps_variation_name";
            this.colps_variation_10_ps_variation_name.MinWidth = 200;
            this.colps_variation_10_ps_variation_name.Name = "colps_variation_10_ps_variation_name";
            this.colps_variation_10_ps_variation_name.Visible = true;
            this.colps_variation_10_ps_variation_name.VisibleIndex = 46;
            this.colps_variation_10_ps_variation_name.Width = 500;
            // 
            // colps_variation_10_ps_variation_price
            // 
            this.colps_variation_10_ps_variation_price.FieldName = "ps_variation_10_ps_variation_price";
            this.colps_variation_10_ps_variation_price.MinWidth = 200;
            this.colps_variation_10_ps_variation_price.Name = "colps_variation_10_ps_variation_price";
            this.colps_variation_10_ps_variation_price.Visible = true;
            this.colps_variation_10_ps_variation_price.VisibleIndex = 47;
            this.colps_variation_10_ps_variation_price.Width = 500;
            // 
            // colps_variation_10_ps_variation_stock
            // 
            this.colps_variation_10_ps_variation_stock.FieldName = "ps_variation_10_ps_variation_stock";
            this.colps_variation_10_ps_variation_stock.MinWidth = 200;
            this.colps_variation_10_ps_variation_stock.Name = "colps_variation_10_ps_variation_stock";
            this.colps_variation_10_ps_variation_stock.Visible = true;
            this.colps_variation_10_ps_variation_stock.VisibleIndex = 48;
            this.colps_variation_10_ps_variation_stock.Width = 500;
            // 
            // colps_variation_11_ps_variation_sku
            // 
            this.colps_variation_11_ps_variation_sku.FieldName = "ps_variation_11_ps_variation_sku";
            this.colps_variation_11_ps_variation_sku.MinWidth = 200;
            this.colps_variation_11_ps_variation_sku.Name = "colps_variation_11_ps_variation_sku";
            this.colps_variation_11_ps_variation_sku.Visible = true;
            this.colps_variation_11_ps_variation_sku.VisibleIndex = 49;
            this.colps_variation_11_ps_variation_sku.Width = 500;
            // 
            // colps_variation_11_ps_variation_name
            // 
            this.colps_variation_11_ps_variation_name.FieldName = "ps_variation_11_ps_variation_name";
            this.colps_variation_11_ps_variation_name.MinWidth = 200;
            this.colps_variation_11_ps_variation_name.Name = "colps_variation_11_ps_variation_name";
            this.colps_variation_11_ps_variation_name.Visible = true;
            this.colps_variation_11_ps_variation_name.VisibleIndex = 50;
            this.colps_variation_11_ps_variation_name.Width = 500;
            // 
            // colps_variation_11_ps_variation_price
            // 
            this.colps_variation_11_ps_variation_price.FieldName = "ps_variation_11_ps_variation_price";
            this.colps_variation_11_ps_variation_price.MinWidth = 200;
            this.colps_variation_11_ps_variation_price.Name = "colps_variation_11_ps_variation_price";
            this.colps_variation_11_ps_variation_price.Visible = true;
            this.colps_variation_11_ps_variation_price.VisibleIndex = 51;
            this.colps_variation_11_ps_variation_price.Width = 500;
            // 
            // colps_variation_11_ps_variation_stock
            // 
            this.colps_variation_11_ps_variation_stock.FieldName = "ps_variation_11_ps_variation_stock";
            this.colps_variation_11_ps_variation_stock.MinWidth = 200;
            this.colps_variation_11_ps_variation_stock.Name = "colps_variation_11_ps_variation_stock";
            this.colps_variation_11_ps_variation_stock.Visible = true;
            this.colps_variation_11_ps_variation_stock.VisibleIndex = 52;
            this.colps_variation_11_ps_variation_stock.Width = 500;
            // 
            // colps_variation_12_ps_variation_sku
            // 
            this.colps_variation_12_ps_variation_sku.FieldName = "ps_variation_12_ps_variation_sku";
            this.colps_variation_12_ps_variation_sku.MinWidth = 200;
            this.colps_variation_12_ps_variation_sku.Name = "colps_variation_12_ps_variation_sku";
            this.colps_variation_12_ps_variation_sku.Visible = true;
            this.colps_variation_12_ps_variation_sku.VisibleIndex = 53;
            this.colps_variation_12_ps_variation_sku.Width = 500;
            // 
            // colps_variation_12_ps_variation_name
            // 
            this.colps_variation_12_ps_variation_name.FieldName = "ps_variation_12_ps_variation_name";
            this.colps_variation_12_ps_variation_name.MinWidth = 200;
            this.colps_variation_12_ps_variation_name.Name = "colps_variation_12_ps_variation_name";
            this.colps_variation_12_ps_variation_name.Visible = true;
            this.colps_variation_12_ps_variation_name.VisibleIndex = 54;
            this.colps_variation_12_ps_variation_name.Width = 500;
            // 
            // colps_variation_12_ps_variation_price
            // 
            this.colps_variation_12_ps_variation_price.FieldName = "ps_variation_12_ps_variation_price";
            this.colps_variation_12_ps_variation_price.MinWidth = 200;
            this.colps_variation_12_ps_variation_price.Name = "colps_variation_12_ps_variation_price";
            this.colps_variation_12_ps_variation_price.Visible = true;
            this.colps_variation_12_ps_variation_price.VisibleIndex = 55;
            this.colps_variation_12_ps_variation_price.Width = 500;
            // 
            // colps_variation_12_ps_variation_stock
            // 
            this.colps_variation_12_ps_variation_stock.FieldName = "ps_variation_12_ps_variation_stock";
            this.colps_variation_12_ps_variation_stock.MinWidth = 200;
            this.colps_variation_12_ps_variation_stock.Name = "colps_variation_12_ps_variation_stock";
            this.colps_variation_12_ps_variation_stock.Visible = true;
            this.colps_variation_12_ps_variation_stock.VisibleIndex = 56;
            this.colps_variation_12_ps_variation_stock.Width = 500;
            // 
            // colps_variation_13_ps_variation_sku
            // 
            this.colps_variation_13_ps_variation_sku.FieldName = "ps_variation_13_ps_variation_sku";
            this.colps_variation_13_ps_variation_sku.MinWidth = 200;
            this.colps_variation_13_ps_variation_sku.Name = "colps_variation_13_ps_variation_sku";
            this.colps_variation_13_ps_variation_sku.Visible = true;
            this.colps_variation_13_ps_variation_sku.VisibleIndex = 57;
            this.colps_variation_13_ps_variation_sku.Width = 500;
            // 
            // colps_variation_13_ps_variation_name
            // 
            this.colps_variation_13_ps_variation_name.FieldName = "ps_variation_13_ps_variation_name";
            this.colps_variation_13_ps_variation_name.MinWidth = 200;
            this.colps_variation_13_ps_variation_name.Name = "colps_variation_13_ps_variation_name";
            this.colps_variation_13_ps_variation_name.Visible = true;
            this.colps_variation_13_ps_variation_name.VisibleIndex = 58;
            this.colps_variation_13_ps_variation_name.Width = 500;
            // 
            // colps_variation_13_ps_variation_price
            // 
            this.colps_variation_13_ps_variation_price.FieldName = "ps_variation_13_ps_variation_price";
            this.colps_variation_13_ps_variation_price.MinWidth = 200;
            this.colps_variation_13_ps_variation_price.Name = "colps_variation_13_ps_variation_price";
            this.colps_variation_13_ps_variation_price.Visible = true;
            this.colps_variation_13_ps_variation_price.VisibleIndex = 59;
            this.colps_variation_13_ps_variation_price.Width = 500;
            // 
            // colps_variation_13_ps_variation_stock
            // 
            this.colps_variation_13_ps_variation_stock.FieldName = "ps_variation_13_ps_variation_stock";
            this.colps_variation_13_ps_variation_stock.MinWidth = 200;
            this.colps_variation_13_ps_variation_stock.Name = "colps_variation_13_ps_variation_stock";
            this.colps_variation_13_ps_variation_stock.Visible = true;
            this.colps_variation_13_ps_variation_stock.VisibleIndex = 60;
            this.colps_variation_13_ps_variation_stock.Width = 500;
            // 
            // colps_variation_14_ps_variation_sku
            // 
            this.colps_variation_14_ps_variation_sku.FieldName = "ps_variation_14_ps_variation_sku";
            this.colps_variation_14_ps_variation_sku.MinWidth = 200;
            this.colps_variation_14_ps_variation_sku.Name = "colps_variation_14_ps_variation_sku";
            this.colps_variation_14_ps_variation_sku.Visible = true;
            this.colps_variation_14_ps_variation_sku.VisibleIndex = 61;
            this.colps_variation_14_ps_variation_sku.Width = 500;
            // 
            // colps_variation_14_ps_variation_name
            // 
            this.colps_variation_14_ps_variation_name.FieldName = "ps_variation_14_ps_variation_name";
            this.colps_variation_14_ps_variation_name.MinWidth = 200;
            this.colps_variation_14_ps_variation_name.Name = "colps_variation_14_ps_variation_name";
            this.colps_variation_14_ps_variation_name.Visible = true;
            this.colps_variation_14_ps_variation_name.VisibleIndex = 62;
            this.colps_variation_14_ps_variation_name.Width = 500;
            // 
            // colps_variation_14_ps_variation_price
            // 
            this.colps_variation_14_ps_variation_price.FieldName = "ps_variation_14_ps_variation_price";
            this.colps_variation_14_ps_variation_price.MinWidth = 200;
            this.colps_variation_14_ps_variation_price.Name = "colps_variation_14_ps_variation_price";
            this.colps_variation_14_ps_variation_price.Visible = true;
            this.colps_variation_14_ps_variation_price.VisibleIndex = 63;
            this.colps_variation_14_ps_variation_price.Width = 500;
            // 
            // colps_variation_14_ps_variation_stock
            // 
            this.colps_variation_14_ps_variation_stock.FieldName = "ps_variation_14_ps_variation_stock";
            this.colps_variation_14_ps_variation_stock.MinWidth = 200;
            this.colps_variation_14_ps_variation_stock.Name = "colps_variation_14_ps_variation_stock";
            this.colps_variation_14_ps_variation_stock.Visible = true;
            this.colps_variation_14_ps_variation_stock.VisibleIndex = 64;
            this.colps_variation_14_ps_variation_stock.Width = 500;
            // 
            // colps_variation_15_ps_variation_sku
            // 
            this.colps_variation_15_ps_variation_sku.FieldName = "ps_variation_15_ps_variation_sku";
            this.colps_variation_15_ps_variation_sku.MinWidth = 200;
            this.colps_variation_15_ps_variation_sku.Name = "colps_variation_15_ps_variation_sku";
            this.colps_variation_15_ps_variation_sku.Visible = true;
            this.colps_variation_15_ps_variation_sku.VisibleIndex = 65;
            this.colps_variation_15_ps_variation_sku.Width = 500;
            // 
            // colps_variation_15_ps_variation_name
            // 
            this.colps_variation_15_ps_variation_name.FieldName = "ps_variation_15_ps_variation_name";
            this.colps_variation_15_ps_variation_name.MinWidth = 200;
            this.colps_variation_15_ps_variation_name.Name = "colps_variation_15_ps_variation_name";
            this.colps_variation_15_ps_variation_name.Visible = true;
            this.colps_variation_15_ps_variation_name.VisibleIndex = 66;
            this.colps_variation_15_ps_variation_name.Width = 500;
            // 
            // colps_variation_15_ps_variation_price
            // 
            this.colps_variation_15_ps_variation_price.FieldName = "ps_variation_15_ps_variation_price";
            this.colps_variation_15_ps_variation_price.MinWidth = 200;
            this.colps_variation_15_ps_variation_price.Name = "colps_variation_15_ps_variation_price";
            this.colps_variation_15_ps_variation_price.Visible = true;
            this.colps_variation_15_ps_variation_price.VisibleIndex = 67;
            this.colps_variation_15_ps_variation_price.Width = 500;
            // 
            // colps_variation_15_ps_variation_stock
            // 
            this.colps_variation_15_ps_variation_stock.FieldName = "ps_variation_15_ps_variation_stock";
            this.colps_variation_15_ps_variation_stock.MinWidth = 200;
            this.colps_variation_15_ps_variation_stock.Name = "colps_variation_15_ps_variation_stock";
            this.colps_variation_15_ps_variation_stock.Visible = true;
            this.colps_variation_15_ps_variation_stock.VisibleIndex = 68;
            this.colps_variation_15_ps_variation_stock.Width = 500;
            // 
            // colps_variation_16_ps_variation_sku
            // 
            this.colps_variation_16_ps_variation_sku.FieldName = "ps_variation_16_ps_variation_sku";
            this.colps_variation_16_ps_variation_sku.MinWidth = 200;
            this.colps_variation_16_ps_variation_sku.Name = "colps_variation_16_ps_variation_sku";
            this.colps_variation_16_ps_variation_sku.Visible = true;
            this.colps_variation_16_ps_variation_sku.VisibleIndex = 69;
            this.colps_variation_16_ps_variation_sku.Width = 500;
            // 
            // colps_variation_16_ps_variation_name
            // 
            this.colps_variation_16_ps_variation_name.FieldName = "ps_variation_16_ps_variation_name";
            this.colps_variation_16_ps_variation_name.MinWidth = 200;
            this.colps_variation_16_ps_variation_name.Name = "colps_variation_16_ps_variation_name";
            this.colps_variation_16_ps_variation_name.Visible = true;
            this.colps_variation_16_ps_variation_name.VisibleIndex = 70;
            this.colps_variation_16_ps_variation_name.Width = 500;
            // 
            // colps_variation_16_ps_variation_price
            // 
            this.colps_variation_16_ps_variation_price.FieldName = "ps_variation_16_ps_variation_price";
            this.colps_variation_16_ps_variation_price.MinWidth = 200;
            this.colps_variation_16_ps_variation_price.Name = "colps_variation_16_ps_variation_price";
            this.colps_variation_16_ps_variation_price.Visible = true;
            this.colps_variation_16_ps_variation_price.VisibleIndex = 71;
            this.colps_variation_16_ps_variation_price.Width = 500;
            // 
            // colps_variation_16_ps_variation_stock
            // 
            this.colps_variation_16_ps_variation_stock.FieldName = "ps_variation_16_ps_variation_stock";
            this.colps_variation_16_ps_variation_stock.MinWidth = 200;
            this.colps_variation_16_ps_variation_stock.Name = "colps_variation_16_ps_variation_stock";
            this.colps_variation_16_ps_variation_stock.Visible = true;
            this.colps_variation_16_ps_variation_stock.VisibleIndex = 72;
            this.colps_variation_16_ps_variation_stock.Width = 500;
            // 
            // colps_variation_17_ps_variation_sku
            // 
            this.colps_variation_17_ps_variation_sku.FieldName = "ps_variation_17_ps_variation_sku";
            this.colps_variation_17_ps_variation_sku.MinWidth = 200;
            this.colps_variation_17_ps_variation_sku.Name = "colps_variation_17_ps_variation_sku";
            this.colps_variation_17_ps_variation_sku.Visible = true;
            this.colps_variation_17_ps_variation_sku.VisibleIndex = 73;
            this.colps_variation_17_ps_variation_sku.Width = 500;
            // 
            // colps_variation_17_ps_variation_name
            // 
            this.colps_variation_17_ps_variation_name.FieldName = "ps_variation_17_ps_variation_name";
            this.colps_variation_17_ps_variation_name.MinWidth = 200;
            this.colps_variation_17_ps_variation_name.Name = "colps_variation_17_ps_variation_name";
            this.colps_variation_17_ps_variation_name.Visible = true;
            this.colps_variation_17_ps_variation_name.VisibleIndex = 74;
            this.colps_variation_17_ps_variation_name.Width = 500;
            // 
            // colps_variation_17_ps_variation_price
            // 
            this.colps_variation_17_ps_variation_price.FieldName = "ps_variation_17_ps_variation_price";
            this.colps_variation_17_ps_variation_price.MinWidth = 200;
            this.colps_variation_17_ps_variation_price.Name = "colps_variation_17_ps_variation_price";
            this.colps_variation_17_ps_variation_price.Visible = true;
            this.colps_variation_17_ps_variation_price.VisibleIndex = 75;
            this.colps_variation_17_ps_variation_price.Width = 500;
            // 
            // colps_variation_17_ps_variation_stock
            // 
            this.colps_variation_17_ps_variation_stock.FieldName = "ps_variation_17_ps_variation_stock";
            this.colps_variation_17_ps_variation_stock.MinWidth = 200;
            this.colps_variation_17_ps_variation_stock.Name = "colps_variation_17_ps_variation_stock";
            this.colps_variation_17_ps_variation_stock.Visible = true;
            this.colps_variation_17_ps_variation_stock.VisibleIndex = 76;
            this.colps_variation_17_ps_variation_stock.Width = 500;
            // 
            // colps_variation_18_ps_variation_sku
            // 
            this.colps_variation_18_ps_variation_sku.FieldName = "ps_variation_18_ps_variation_sku";
            this.colps_variation_18_ps_variation_sku.MinWidth = 200;
            this.colps_variation_18_ps_variation_sku.Name = "colps_variation_18_ps_variation_sku";
            this.colps_variation_18_ps_variation_sku.Visible = true;
            this.colps_variation_18_ps_variation_sku.VisibleIndex = 77;
            this.colps_variation_18_ps_variation_sku.Width = 500;
            // 
            // colps_variation_18_ps_variation_name
            // 
            this.colps_variation_18_ps_variation_name.FieldName = "ps_variation_18_ps_variation_name";
            this.colps_variation_18_ps_variation_name.MinWidth = 200;
            this.colps_variation_18_ps_variation_name.Name = "colps_variation_18_ps_variation_name";
            this.colps_variation_18_ps_variation_name.Visible = true;
            this.colps_variation_18_ps_variation_name.VisibleIndex = 78;
            this.colps_variation_18_ps_variation_name.Width = 500;
            // 
            // colps_variation_18_ps_variation_price
            // 
            this.colps_variation_18_ps_variation_price.FieldName = "ps_variation_18_ps_variation_price";
            this.colps_variation_18_ps_variation_price.MinWidth = 200;
            this.colps_variation_18_ps_variation_price.Name = "colps_variation_18_ps_variation_price";
            this.colps_variation_18_ps_variation_price.Visible = true;
            this.colps_variation_18_ps_variation_price.VisibleIndex = 79;
            this.colps_variation_18_ps_variation_price.Width = 500;
            // 
            // colps_variation_18_ps_variation_stock
            // 
            this.colps_variation_18_ps_variation_stock.FieldName = "ps_variation_18_ps_variation_stock";
            this.colps_variation_18_ps_variation_stock.MinWidth = 200;
            this.colps_variation_18_ps_variation_stock.Name = "colps_variation_18_ps_variation_stock";
            this.colps_variation_18_ps_variation_stock.Visible = true;
            this.colps_variation_18_ps_variation_stock.VisibleIndex = 80;
            this.colps_variation_18_ps_variation_stock.Width = 500;
            // 
            // colps_variation_19_ps_variation_sku
            // 
            this.colps_variation_19_ps_variation_sku.FieldName = "ps_variation_19_ps_variation_sku";
            this.colps_variation_19_ps_variation_sku.MinWidth = 200;
            this.colps_variation_19_ps_variation_sku.Name = "colps_variation_19_ps_variation_sku";
            this.colps_variation_19_ps_variation_sku.Visible = true;
            this.colps_variation_19_ps_variation_sku.VisibleIndex = 81;
            this.colps_variation_19_ps_variation_sku.Width = 500;
            // 
            // colps_variation_19_ps_variation_name
            // 
            this.colps_variation_19_ps_variation_name.FieldName = "ps_variation_19_ps_variation_name";
            this.colps_variation_19_ps_variation_name.MinWidth = 200;
            this.colps_variation_19_ps_variation_name.Name = "colps_variation_19_ps_variation_name";
            this.colps_variation_19_ps_variation_name.Visible = true;
            this.colps_variation_19_ps_variation_name.VisibleIndex = 82;
            this.colps_variation_19_ps_variation_name.Width = 500;
            // 
            // colps_variation_19_ps_variation_price
            // 
            this.colps_variation_19_ps_variation_price.FieldName = "ps_variation_19_ps_variation_price";
            this.colps_variation_19_ps_variation_price.MinWidth = 200;
            this.colps_variation_19_ps_variation_price.Name = "colps_variation_19_ps_variation_price";
            this.colps_variation_19_ps_variation_price.Visible = true;
            this.colps_variation_19_ps_variation_price.VisibleIndex = 83;
            this.colps_variation_19_ps_variation_price.Width = 500;
            // 
            // colps_variation_19_ps_variation_stock
            // 
            this.colps_variation_19_ps_variation_stock.FieldName = "ps_variation_19_ps_variation_stock";
            this.colps_variation_19_ps_variation_stock.MinWidth = 200;
            this.colps_variation_19_ps_variation_stock.Name = "colps_variation_19_ps_variation_stock";
            this.colps_variation_19_ps_variation_stock.Visible = true;
            this.colps_variation_19_ps_variation_stock.VisibleIndex = 84;
            this.colps_variation_19_ps_variation_stock.Width = 500;
            // 
            // colps_variation_20_ps_variation_sku
            // 
            this.colps_variation_20_ps_variation_sku.FieldName = "ps_variation_20_ps_variation_sku";
            this.colps_variation_20_ps_variation_sku.MinWidth = 200;
            this.colps_variation_20_ps_variation_sku.Name = "colps_variation_20_ps_variation_sku";
            this.colps_variation_20_ps_variation_sku.Visible = true;
            this.colps_variation_20_ps_variation_sku.VisibleIndex = 85;
            this.colps_variation_20_ps_variation_sku.Width = 500;
            // 
            // colps_variation_20_ps_variation_name
            // 
            this.colps_variation_20_ps_variation_name.FieldName = "ps_variation_20_ps_variation_name";
            this.colps_variation_20_ps_variation_name.MinWidth = 200;
            this.colps_variation_20_ps_variation_name.Name = "colps_variation_20_ps_variation_name";
            this.colps_variation_20_ps_variation_name.Visible = true;
            this.colps_variation_20_ps_variation_name.VisibleIndex = 86;
            this.colps_variation_20_ps_variation_name.Width = 500;
            // 
            // colps_variation_20_ps_variation_price
            // 
            this.colps_variation_20_ps_variation_price.FieldName = "ps_variation_20_ps_variation_price";
            this.colps_variation_20_ps_variation_price.MinWidth = 200;
            this.colps_variation_20_ps_variation_price.Name = "colps_variation_20_ps_variation_price";
            this.colps_variation_20_ps_variation_price.Visible = true;
            this.colps_variation_20_ps_variation_price.VisibleIndex = 87;
            this.colps_variation_20_ps_variation_price.Width = 500;
            // 
            // colps_variation_20_ps_variation_stock
            // 
            this.colps_variation_20_ps_variation_stock.FieldName = "ps_variation_20_ps_variation_stock";
            this.colps_variation_20_ps_variation_stock.MinWidth = 200;
            this.colps_variation_20_ps_variation_stock.Name = "colps_variation_20_ps_variation_stock";
            this.colps_variation_20_ps_variation_stock.Visible = true;
            this.colps_variation_20_ps_variation_stock.VisibleIndex = 88;
            this.colps_variation_20_ps_variation_stock.Width = 500;
            // 
            // colps_img_1
            // 
            this.colps_img_1.FieldName = "ps_img_1";
            this.colps_img_1.MinWidth = 200;
            this.colps_img_1.Name = "colps_img_1";
            this.colps_img_1.Visible = true;
            this.colps_img_1.VisibleIndex = 89;
            this.colps_img_1.Width = 500;
            // 
            // colps_img_2
            // 
            this.colps_img_2.FieldName = "ps_img_2";
            this.colps_img_2.MinWidth = 200;
            this.colps_img_2.Name = "colps_img_2";
            this.colps_img_2.Visible = true;
            this.colps_img_2.VisibleIndex = 90;
            this.colps_img_2.Width = 500;
            // 
            // colps_img_3
            // 
            this.colps_img_3.FieldName = "ps_img_3";
            this.colps_img_3.MinWidth = 200;
            this.colps_img_3.Name = "colps_img_3";
            this.colps_img_3.Visible = true;
            this.colps_img_3.VisibleIndex = 91;
            this.colps_img_3.Width = 500;
            // 
            // colps_img_4
            // 
            this.colps_img_4.FieldName = "ps_img_4";
            this.colps_img_4.MinWidth = 200;
            this.colps_img_4.Name = "colps_img_4";
            this.colps_img_4.Visible = true;
            this.colps_img_4.VisibleIndex = 92;
            this.colps_img_4.Width = 500;
            // 
            // colps_img_5
            // 
            this.colps_img_5.FieldName = "ps_img_5";
            this.colps_img_5.MinWidth = 200;
            this.colps_img_5.Name = "colps_img_5";
            this.colps_img_5.Visible = true;
            this.colps_img_5.VisibleIndex = 93;
            this.colps_img_5.Width = 500;
            // 
            // colps_img_6
            // 
            this.colps_img_6.FieldName = "ps_img_6";
            this.colps_img_6.MinWidth = 200;
            this.colps_img_6.Name = "colps_img_6";
            this.colps_img_6.Visible = true;
            this.colps_img_6.VisibleIndex = 94;
            this.colps_img_6.Width = 500;
            // 
            // colps_img_7
            // 
            this.colps_img_7.FieldName = "ps_img_7";
            this.colps_img_7.MinWidth = 200;
            this.colps_img_7.Name = "colps_img_7";
            this.colps_img_7.Visible = true;
            this.colps_img_7.VisibleIndex = 95;
            this.colps_img_7.Width = 500;
            // 
            // colps_img_8
            // 
            this.colps_img_8.FieldName = "ps_img_8";
            this.colps_img_8.MinWidth = 200;
            this.colps_img_8.Name = "colps_img_8";
            this.colps_img_8.Visible = true;
            this.colps_img_8.VisibleIndex = 96;
            this.colps_img_8.Width = 500;
            // 
            // colps_img_9
            // 
            this.colps_img_9.FieldName = "ps_img_9";
            this.colps_img_9.MinWidth = 200;
            this.colps_img_9.Name = "colps_img_9";
            this.colps_img_9.Visible = true;
            this.colps_img_9.VisibleIndex = 97;
            this.colps_img_9.Width = 500;
            // 
            // colps_mass_upload_shipment_help
            // 
            this.colps_mass_upload_shipment_help.FieldName = "ps_mass_upload_shipment_help";
            this.colps_mass_upload_shipment_help.MinWidth = 200;
            this.colps_mass_upload_shipment_help.Name = "colps_mass_upload_shipment_help";
            this.colps_mass_upload_shipment_help.Visible = true;
            this.colps_mass_upload_shipment_help.VisibleIndex = 98;
            this.colps_mass_upload_shipment_help.Width = 500;
            // 
            // colchannel_50010_switch
            // 
            this.colchannel_50010_switch.FieldName = "channel_50010_switch";
            this.colchannel_50010_switch.MinWidth = 200;
            this.colchannel_50010_switch.Name = "colchannel_50010_switch";
            this.colchannel_50010_switch.Visible = true;
            this.colchannel_50010_switch.VisibleIndex = 99;
            this.colchannel_50010_switch.Width = 500;
            // 
            // colchannel_50011_switch
            // 
            this.colchannel_50011_switch.FieldName = "channel_50011_switch";
            this.colchannel_50011_switch.MinWidth = 200;
            this.colchannel_50011_switch.Name = "colchannel_50011_switch";
            this.colchannel_50011_switch.Visible = true;
            this.colchannel_50011_switch.VisibleIndex = 100;
            this.colchannel_50011_switch.Width = 500;
            // 
            // colchannel_50012_switch
            // 
            this.colchannel_50012_switch.FieldName = "channel_50012_switch";
            this.colchannel_50012_switch.MinWidth = 200;
            this.colchannel_50012_switch.Name = "colchannel_50012_switch";
            this.colchannel_50012_switch.Visible = true;
            this.colchannel_50012_switch.VisibleIndex = 101;
            this.colchannel_50012_switch.Width = 500;
            // 
            // colchannel_50066_switch
            // 
            this.colchannel_50066_switch.FieldName = "channel_50066_switch";
            this.colchannel_50066_switch.MinWidth = 200;
            this.colchannel_50066_switch.Name = "colchannel_50066_switch";
            this.colchannel_50066_switch.Visible = true;
            this.colchannel_50066_switch.VisibleIndex = 102;
            this.colchannel_50066_switch.Width = 500;
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            // 
            // btnGetdata
            // 
            this.btnGetdata.Location = new System.Drawing.Point(637, 8);
            this.btnGetdata.Name = "btnGetdata";
            this.btnGetdata.Size = new System.Drawing.Size(75, 23);
            this.btnGetdata.TabIndex = 3;
            this.btnGetdata.Text = "Lấy dữ liệu";
            this.btnGetdata.UseVisualStyleBackColor = true;
            this.btnGetdata.Click += new System.EventHandler(this.btnGetdata_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mã shop";
            // 
            // txtShopId
            // 
            this.txtShopId.Location = new System.Drawing.Point(71, 10);
            this.txtShopId.Name = "txtShopId";
            this.txtShopId.Size = new System.Drawing.Size(151, 20);
            this.txtShopId.TabIndex = 2;
            this.txtShopId.Text = "69781472";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(637, 33);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Xuất dữ liệu";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(225, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Lọc theo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(225, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Sắp xếp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(439, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Giới hạn";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(439, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Trang";
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // txtFromPages
            // 
            this.txtFromPages.Location = new System.Drawing.Point(497, 36);
            this.txtFromPages.Name = "txtFromPages";
            this.txtFromPages.Size = new System.Drawing.Size(50, 20);
            this.txtFromPages.TabIndex = 5;
            this.txtFromPages.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // txtLimit
            // 
            this.txtLimit.Location = new System.Drawing.Point(497, 6);
            this.txtLimit.Name = "txtLimit";
            this.txtLimit.Size = new System.Drawing.Size(120, 20);
            this.txtLimit.TabIndex = 6;
            this.txtLimit.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // txtToPage
            // 
            this.txtToPage.Location = new System.Drawing.Point(578, 35);
            this.txtToPage.Name = "txtToPage";
            this.txtToPage.Size = new System.Drawing.Size(39, 20);
            this.txtToPage.TabIndex = 5;
            this.txtToPage.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(553, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "=>";
            // 
            // txtShopName
            // 
            this.txtShopName.Location = new System.Drawing.Point(71, 35);
            this.txtShopName.Name = "txtShopName";
            this.txtShopName.Properties.Mask.EditMask = "\\S*";
            this.txtShopName.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.txtShopName.Size = new System.Drawing.Size(148, 20);
            this.txtShopName.TabIndex = 8;
            this.txtShopName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtShopName_KeyDown);
            // 
            // txtBy
            // 
            this.txtBy.EditValue = "ctime";
            this.txtBy.Location = new System.Drawing.Point(283, 10);
            this.txtBy.Name = "txtBy";
            this.txtBy.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtBy.Properties.Items.AddRange(new object[] {
            "ctime",
            "pop"});
            this.txtBy.Size = new System.Drawing.Size(151, 20);
            this.txtBy.TabIndex = 9;
            // 
            // txtOrder
            // 
            this.txtOrder.EditValue = "desc";
            this.txtOrder.Location = new System.Drawing.Point(283, 34);
            this.txtOrder.Name = "txtOrder";
            this.txtOrder.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtOrder.Properties.Items.AddRange(new object[] {
            "desc",
            "asc"});
            this.txtOrder.Size = new System.Drawing.Size(151, 20);
            this.txtOrder.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtOrder);
            this.Controls.Add(this.txtBy);
            this.Controls.Add(this.txtShopName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtLimit);
            this.Controls.Add(this.txtToPage);
            this.Controls.Add(this.txtFromPages);
            this.Controls.Add(this.grcData);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnGetdata);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtShopId);
            this.Controls.Add(this.label2);
            this.HelpButton = true;
            this.Name = "Form1";
            this.Text = "Shopee Crawer";
            this.HelpButtonClicked += new System.ComponentModel.CancelEventHandler(this.Form1_HelpButtonClicked);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grcData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bdsData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromPages)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLimit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtToPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtShopName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBy.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtOrder.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnGetdata;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtShopId;
        private DevExpress.XtraGrid.GridControl grcData;
        private DevExpress.XtraGrid.Views.Grid.GridView grvData;
        private System.Windows.Forms.BindingSource bdsData;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit2;
        private System.Windows.Forms.Button button2;
        private DevExpress.XtraGrid.Columns.GridColumn colps_category_list_id;
        private DevExpress.XtraGrid.Columns.GridColumn colps_product_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_product_description;
        private DevExpress.XtraGrid.Columns.GridColumn colps_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_product_weight;
        private DevExpress.XtraGrid.Columns.GridColumn colps_days_to_ship;
        private DevExpress.XtraGrid.Columns.GridColumn colps_sku_ref_no_parent;
        private DevExpress.XtraGrid.Columns.GridColumn colps_mass_upload_variation_help;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_1_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_1_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_1_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_1_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_2_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_2_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_2_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_2_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_3_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_3_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_3_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_3_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_4_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_4_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_4_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_4_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_5_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_5_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_5_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_5_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_6_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_6_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_6_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_6_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_7_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_7_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_7_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_7_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_8_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_8_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_8_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_8_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_9_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_9_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_9_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_9_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_10_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_10_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_10_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_10_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_11_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_11_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_11_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_11_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_12_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_12_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_12_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_12_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_13_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_13_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_13_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_13_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_14_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_14_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_14_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_14_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_15_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_15_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_15_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_15_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_16_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_16_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_16_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_16_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_17_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_17_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_17_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_17_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_18_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_18_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_18_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_18_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_19_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_19_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_19_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_19_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_20_ps_variation_sku;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_20_ps_variation_name;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_20_ps_variation_price;
        private DevExpress.XtraGrid.Columns.GridColumn colps_variation_20_ps_variation_stock;
        private DevExpress.XtraGrid.Columns.GridColumn colps_img_1;
        private DevExpress.XtraGrid.Columns.GridColumn colps_img_2;
        private DevExpress.XtraGrid.Columns.GridColumn colps_img_3;
        private DevExpress.XtraGrid.Columns.GridColumn colps_img_4;
        private DevExpress.XtraGrid.Columns.GridColumn colps_img_5;
        private DevExpress.XtraGrid.Columns.GridColumn colps_img_6;
        private DevExpress.XtraGrid.Columns.GridColumn colps_img_7;
        private DevExpress.XtraGrid.Columns.GridColumn colps_img_8;
        private DevExpress.XtraGrid.Columns.GridColumn colps_img_9;
        private DevExpress.XtraGrid.Columns.GridColumn colps_mass_upload_shipment_help;
        private DevExpress.XtraGrid.Columns.GridColumn colchannel_50010_switch;
        private DevExpress.XtraGrid.Columns.GridColumn colchannel_50011_switch;
        private DevExpress.XtraGrid.Columns.GridColumn colchannel_50012_switch;
        private DevExpress.XtraGrid.Columns.GridColumn colchannel_50066_switch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.Windows.Forms.NumericUpDown txtFromPages;
        private System.Windows.Forms.NumericUpDown txtLimit;
        private System.Windows.Forms.NumericUpDown txtToPage;
        private System.Windows.Forms.Label label6;
        private DevExpress.XtraEditors.TextEdit txtShopName;
        private DevExpress.XtraEditors.ComboBoxEdit txtBy;
        private DevExpress.XtraEditors.ComboBoxEdit txtOrder;
    }
}

